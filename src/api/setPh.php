<?php
include '../inclusioni.php';

$dbMgr=new DbManager();
$operation_phMgr = new OperationPhMgr($dbMgr);
$impiantiMgr = new ImpiantiMgr($dbMgr);

if(isset($_POST['device']) && isset($_POST['ph'])) {

    $dbMgr=new DbManager();
    $dbMgr->startTransaction();

    $device = $_POST['device'];
    
    try{
        $impianti=$impiantiMgr->getImpiantiByIdDevice($device);
        $impianto=$impianti[0];

        $values['id_impianto']=$impianto->getId();
        $values['ph'] = $_POST['ph'];

        $operationPh = $operation_phMgr->insertOperationPh($values);
        
    }catch(Exception $e){
        $operationPh=[];
    }
}
?>