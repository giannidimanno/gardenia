<?php
include '../inclusioni.php';

$dbMgr=new DbManager();
$operation_temperaturaMgr = new OperationTemperaturaMgr($dbMgr);
$impiantiMgr = new ImpiantiMgr($dbMgr);

if(isset($_POST['device']) && isset($_POST['temperatura'])) {

    $dbMgr=new DbManager();
    $dbMgr->startTransaction();

    $device = $_POST['device'];
    
        try{
            $impianti=$impiantiMgr->getImpiantiByIdDevice($device);
            $impianto=$impianti[0];

            $values['id_impianto']=$impianto->getId();
            $values['temperatura'] = $_POST['temperatura'];

            $operationTemp = $operation_temperaturaMgr->insertOperationTemperatura($values);

        }catch(Exception $e){
            $operationTemp=[];
        }

    
}
?>
