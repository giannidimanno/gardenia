<?php
    include '../inclusioni.php';
    $dbMgr=new DbManager();
    $processingRequestMgr = new ProcessingRequestMgr($dbMgr);

    if(isset($_POST['id'])) {

        $dbMgr=new DbManager();
        $dbMgr->startTransaction();

        $id=$_POST['id'];
        $stato = $_POST['stato'];
        $note = $_POST['note'];

        try{
            $processingRequestMgr->updateProcessingRequestStatoById($stato, $note, $id);

        }catch(Exception $e){
            echo "errore";
        }
    }
?>