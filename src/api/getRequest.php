<?php
include '../inclusioni.php';
$dbMgr=new DbManager();
$processingRequestMgr = new ProcessingRequestMgr($dbMgr);
$impiantiMgr = new ImpiantiMgr($dbMgr);

if(isset($_POST['device'])) {

    $dbMgr=new DbManager();
    $dbMgr->startTransaction();

    $device = $_POST['device'];
    
        try{
            $impiantiMgr->getImpiantiByIdDevice($device);
            $processing = $processingRequestMgr->getProcessingRequestByDevice($device);
            
            echo json_encode($processing);

        }catch(Exception $e){
            $processing=[];
        }

    
}
?>



