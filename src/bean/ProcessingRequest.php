<?php

class ProcessingRequest {

    private $id;
    private $azione;
    private $stato;
    private $device;
    private $data_processing;
    private $data_inserimento;
    private $note;
    private $flag_eliminato;
	private $data_eliminazione;

    //Costruttore
    public function __construct($dati){

        $this->id=$dati['id'];
        $this->azione=$dati['azione'];
        $this->stato=$dati['stato'];
        $this->device=$dati['device'];
	    $this->data_processing=$dati['data_processing'];
	    $this->data_inserimento=$dati['data_inserimento'];
        $this->note=$dati['note'];
        $this->flag_eliminato=$dati['flag_eliminato'];	
		$this->data_eliminazione=$dati['data_eliminazione'];	
    }

    //Metodi Get
    public function getId(){ return $this->id; }

    public function getAzione(){ return $this->azione; }

    public function getStato(){ return $this->stato; }

    public function getDevice(){ return $this->device; }

    public function getDataProcessing(){ return $this->data_processing; }

    public function getDataInserimento(){ return $this->data_inserimento; }

    public function getNote(){ return $this->note; }
    
    public function getFlagEliminato(){ return $this->flag_eliminato; }
	
	public function getDataEliminazione(){ return $this->data_eliminazione; }

}

?>