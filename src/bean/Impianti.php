<?php

class Impianti {

    private $id;
    private $nome;
    private $dimensioni;
    private $stato;
    private $id_campo;
    private $device;
    private $flag_eliminato;
	private $data_eliminazione;

    //Costruttore
    public function __construct($dati){

        $this->id=$dati['id'];
        $this->nome=$dati['nome'];
        $this->dimensioni=$dati['dimensioni'];
	    $this->stato=$dati['stato'];
	    $this->id_campo=$dati['id_campo'];
        $this->device=$dati['device'];
        $this->flag_eliminato=$dati['flag_eliminato'];	
		$this->data_eliminazione=$dati['data_eliminazione'];	
    }

    //Metodi Get
    public function getId(){ return $this->id; }

    public function getNome(){ return $this->nome; }

    public function getDimensioni(){ return $this->dimensioni; }

    public function getStato(){ return $this->stato; }

    public function getIdCampo(){ return $this->id_campo; }

    public function getDevice(){ return $this->device; }
    
    public function getFlagEliminato(){
		return $this->flag_eliminato;	
	}
	
	public function getDataEliminazione(){
		return $this->data_eliminazione;	
	}

}
?>