<?php

class Aziende {
    private $id;
    private $ragione_sociale;
    private $indirizzo;
    private $telefono;
    private $codice_fiscale;
    private $partita_iva;
    private $id_utente;
    private $flag_eliminato;
	private $data_eliminazione;

    // Costruttore
    public function __construct($dati) {

        $this->id=$dati['id'];
        $this->ragione_sociale=$dati['ragione_sociale'];
        $this->indirizzo=$dati['indirizzo'];
        $this->telefono=$dati['telefono'];
        $this->codice_fiscale=$dati['codice_fiscale'];
        $this->partita_iva=$dati['partita_iva'];
        $this->id_utente=$dati['id_utente'];
        $this->flag_eliminato=$dati['flag_eliminato'];	
		$this->data_eliminazione=$dati['data_eliminazione'];	
    }

    // Metodi Get
    public function getId(){ return $this->id; }

    public function getRagioneSociale(){ return $this->ragione_sociale; }

    public function getIndirizzo(){ return $this->indirizzo; }

    public function getTelefono(){ return $this->telefono; }

    public function getCodiceFiscale(){ return $this->codice_fiscale; }

    public function getPartitaIva(){ return $this->partita_iva; }

    public function getIdUtente(){ return $this->id_utente; }

    public function getFlagEliminato(){
		return $this->flag_eliminato;	
	}
	
	public function getDataEliminazione(){
		return $this->data_eliminazione;	
	}

}
?>