<?php

class Utenti {

    private $id;
    private $nome;
    private $cognome;
    private $password;
    private $email;
    private $telefono;
    private $amministratore;
    private $flag_eliminato;
	private $data_eliminazione;

    public function __construct($dati) {
        $this->id = $dati['id'];
        $this->nome = $dati['nome'];
        $this->cognome = $dati['cognome'];
        $this->password = $dati['password'];
        $this->email = $dati['email'];
        $this->telefono = $dati['telefono'];
        $this->amministratore = $dati['amministratore'];
        $this->flag_eliminato=$dati['flag_eliminato'];	
		$this->data_eliminazione=$dati['data_eliminazione'];	
    }

    // metodi get

    public function getId(){ return $this->id; }
    public function getNome(){ return $this->nome; }
    public function getCognome(){ return $this->cognome; }
    public function getPassword(){ return $this->password; }
    public function getEmail(){ return $this->email; }
    public function getTelefono(){ return $this->telefono; }
    public function getAmministratore(){ return $this->amministratore; }
    public function getFlagEliminato(){
		return $this->flag_eliminato;	
	}
	
	public function getDataEliminazione(){
		return $this->data_eliminazione;	
	}
}

?>