<?php

class Campi {

    private $id;
    private $luogo;
    private $latitudine;
    private $longitudine;
    private $dimensione;
    private $id_azienda;
    private $flag_eliminato;
	private $data_eliminazione;


    //Costruttore
    public function __construct($dati){

        $this->id=$dati['id'];
        $this->luogo=$dati['luogo'];
        $this->latitudine=$dati['latitudine'];
        $this->longitudine=$dati['longitudine'];
        $this->dimensione=$dati['dimensione'];
        $this->id_azienda=$dati['id_azienda'];
        $this->flag_eliminato=$dati['flag_eliminato'];	
		$this->data_eliminazione=$dati['data_eliminazione'];	
    }

    //Metodi Get
    public function getId(){ return $this->id; }

    public function getLuogo(){ return $this->luogo; }

    public function getLatitudine(){ return $this->latitudine; }

    public function getLongitudine(){ return $this->longitudine; }

    public function getDimensione(){ return $this->dimensione; }

    public function getIdAzienda(){ return $this->id_azienda; }

    public function getFlagEliminato(){
		return $this->flag_eliminato;	
	}
	
	public function getDataEliminazione(){
		return $this->data_eliminazione;	
	}
}
?>
