<?php

class OperationTemperatura {

    private $id;
    private $temperatura;
    private $data_acquisizione;
    private $id_impianto;

    

    //Costruttore
    public function __construct($dati){

        $this->id=$dati['id'];
        $this->temperatura=$dati['temperatura'];
        $this->data_acquisizione=$dati['data_acquisizione'];
        $this->id_impianto=$dati['id_impianto'];
    }

    //Metodi Get
    public function getId(){ return $this->id; }

    public function getTemperatura(){ return $this->temperatura; }

    public function getData_acquisizione(){ return $this->data_acquisizione; }

    public function getId_impianto(){ return $this->id_impianto; }
}

?>