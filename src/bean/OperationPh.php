<?php

class OperationPh {

    private $id;
    private $ph;
    private $data_acquisizione;
    private $id_impianto;

    

    //Costruttore
    public function __construct($dati){

        $this->id=$dati['id'];
        $this->ph=$dati['ph'];
        $this->data_acquisizione=$dati['data_acquisizione'];
        $this->id_impianto=$dati['id_impianto'];
    }

    //Metodi Get
    public function getId(){ return $this->id; }

    public function getPh(){ return $this->ph; }

    public function getData_acquisizione(){ return $this->data_acquisizione; }

    public function getId_impianto(){ return $this->id_impianto; }
}

?>