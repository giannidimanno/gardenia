<?php
include 'inclusioni.php';
try{
    $dbMgr=new DbManager();
    $dbMgr->startTransaction();
    $aziendeMgr = new AziendeMgr($dbMgr);
    $campiMgr = new CampiMgr($dbMgr);
    $impiantiMgr = new ImpiantiMgr($dbMgr);

    $id=$_GET['id'];
    $aziendeMgr->deleteAziendaById($id);

    $campi=$campiMgr->getCampiByIdAzienda($id);
    foreach($campi as $campo){
        $campiMgr->deleteById($campo->getid());
    }

    $impianti = $impiantiMgr->getImpiantiByIdAzienda($id);
    foreach($impianti as $impianto){
        $impiantiMgr->deleteImpiantiById($impianto->getid());
    }

    $dbMgr->commit();
    header('Location: index.php?link=12');
    exit;
} catch (Exception $e) {
    $dbMgr->rollback();
    header('Location: logout.php');
    exit;
}


?>
