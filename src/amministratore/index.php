<?php
include 'inclusioni.php';
include 'permission.php';
include 'head.php';

?>

<body id="page-top">

    <div class="loader"></div>

    <!-- Page Wrapper -->
    <div id="wrapper">
  
	    <!-- Sidebar -->
	    <?php include 'sidebar.php';?>

        <!-- Content Wrapper -->
        <div id="content-wrapper" class="d-flex flex-column">

            <!-- Main Content -->
            <div id="content">

                <!-- Topbar -->
                <?php include 'topbar.php';?>

                <div class="container-fluid" style="min-height:80%">

                    <!-- Main Content -->
                    <?php include 'main_content.php';?>

                </div>

            </div>
            <!-- End of Main Content -->

            <!-- Footer -->
            <footer class="sticky-footer bg-white">
                <div class="container my-auto">
                    <div class="copyright text-center my-auto">
                        <span>Copyright &copy; Idroponica - 04022 Fondi (LT)</span>
                    </div>
                </div>
            </footer>
            <!-- End of Footer -->

        </div>
        <!-- End of Content Wrapper -->

  </div>
  <!-- End of Page Wrapper -->

  <!-- Scroll to Top Button-->
  <a class="scroll-to-top rounded" href="#page-top">
    <i class="fas fa-angle-up"></i>
  </a>

  <!-- Logout Modal-->
  <div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Sicuro di voler uscire da questa console?</h5>
          <button class="close" type="button" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">×</span>
          </button>
        </div>
        <div class="modal-body">Scegli "Logout" se sei sicuro di voler abbandonare questa console.</div>
        <div class="modal-footer">
          <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancella</button>
          <a class="btn btn-primary" href="../logout.php">Logout</a>
        </div>
      </div>
    </div>
  </div>


    <!-- Delete Campo Modal-->
    <div class="modal fade modaleCampo" id="modaleCampo" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Sicuro di voler cancellare questo campo?</h5>
                    <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body">Scegli "Yes" se sei sicuro di voler cancellare questo campo.</div>
                <div class="modal-footer">
                    <button class="btn btn-secondary" type="button" data-dismiss="modal">No</button>
                    <button class="btn btn-primary" type="button" data-dismiss="model" onclick="deleteCampo()" id="button-modal-value">Yes</button>
                    <input type="hidden" name="button-modal-value" id="button-modal-value-delete-campo" value=""/>
                </div>
            </div>
        </div>
    </div>

    <!-- Delete Utente Modal-->
    <div class="modal fade modaleutente" id="modaleUtente" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Sicuro di voler cancellare questo utente?</h5>
                    <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body">Scegli "Yes" se sei sicuro di voler cancellare questo utente.</div>
                <div class="modal-footer">
                    <button class="btn btn-secondary" type="button" data-dismiss="modal">No</button>
                    <button class="btn btn-primary" type="button" data-dismiss="model" onclick="deleteUtenti()" id="button-modal-value">Yes</button>
                    <input type="hidden" name="button-modal-value" id="button-modal-value-delete-utente" value=""/>
                </div>
            </div>
        </div>
    </div>


    <!-- Delete impianto Modal-->
    <div class="modal fade modaleimpianto" id="modaleimpianto" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Sicuro di voler cancellare questa impianto?</h5>
                    <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body">Scegli "Yes" se sei sicuro di voler cancellare questa impianto.</div>
                <div class="modal-footer">
                    <button class="btn btn-secondary" type="button" data-dismiss="modal">No</button>
                    <button class="btn btn-primary" type="button" data-dismiss="model" onclick="deleteImpianti()" id="button-modal-value">Yes</button>
                    <input type="hidden" name="button-modal-value" id="button-modal-value-delete-impianto" value=""/>
                </div>
            </div>
        </div>
    </div>


     <!-- Delete Azienda Modal-->
     <div class="modal fade modaleAzienda" id="modaleAzienda" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Sicuro di voler cancellare questa azienda?</h5>
                    <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body">Scegli "Yes" se sei sicuro di voler cancellare questa azienda.</div>
                <div class="modal-footer">
                    <button class="btn btn-secondary" type="button" data-dismiss="modal">No</button>
                    <button class="btn btn-primary" type="button" data-dismiss="model" onclick="deleteAzienda()" id="button-modal-value">Yes</button>
                    <input type="hidden" name="button-modal-value" id="button-modal-value-delete-azienda" value=""/>
                </div>
            </div>
        </div>
    </div>


  <!-- Bootstrap core JavaScript-->
  <!-- <script src="vendor/jquery/jquery.min.js"></script>-->
  <script src="../vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

  <!-- Core plugin JavaScript-->
  <script src="../vendor/jquery-easing/jquery.easing.min.js"></script>

  <!-- Custom scripts for all pages-->
  <script src="../js/sb-admin-2.js?versione=<?php echo time(); ?>"></script>


  <!-- Page level plugins -->
  <script src="../vendor/datatables/jquery.dataTables.min.js"></script>
  <script src="../vendor/datatables/dataTables.bootstrap4.min.js"></script>

  <!-- Page level custom scripts -->
  <script src="../js/demo/datatables-demo.js"></script>


    <?php
    if(!isset($_GET['link'])){
        header('Location: logout.php');die();
    }else{
        $rooting=$_GET['link'];
    }
    if($rooting==1) {
        echo "<script>google.maps.event.addDomListener(window, 'load', init)</script>";
    }elseif($rooting==3){
        echo "<script>google.maps.event.addDomListener(window, 'load', init)</script>";
        echo "<script>google.maps.event.addDomListener(window, 'load', showMaps)</script>";
    }



    ?>





</body>

</html>
