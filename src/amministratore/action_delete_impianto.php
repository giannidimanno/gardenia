<?php
include 'inclusioni.php';
try{
    $dbMgr=new DbManager();
    $dbMgr->startTransaction();
    $impiantiMgr = new impiantiMgr($dbMgr);
    $id=$_GET['id'];
    $impiantiMgr->deleteImpiantiById($id);
    $dbMgr->commit();
    header('Location: index.php?link=6');
    exit;
} catch (Exception $e) {
    $dbMgr->rollback();
    header('Location: logout.php');
    exit;
}


?>
