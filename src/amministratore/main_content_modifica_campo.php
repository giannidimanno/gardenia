<?php

$dbMgr=new DbManager();
$campiMgr = new CampiMgr($dbMgr);
$aziendaMgr = new AziendeMgr($dbMgr);

$id_campo=$_GET['id_campo'];

if(isset($_REQUEST['submitModificaCampo'])){

    $values['luogo'] = $_POST['luogo'];
    $values['latitudine'] = $_POST['latitudine'];
    $values['longitudine'] = $_POST['longitudine'];
    $values['dimensione'] = $_POST['dimensione'];
    $values['id_azienda'] = $_POST['id_azienda'];

    try{
        $dbMgr->startTransaction();

        $campiMgr->updateCampoById($id_campo,$values);
        $dbMgr->commit();
        header('Location: index.php?link=2');
        exit;
    } catch (Exception $e) {
        $dbMgr->rollback();
    }

}

$campi=$campiMgr->getCampoById($id_campo);
$campo=$campi[0];
$aziende=$aziendaMgr->getAziendaById($campo->getIdAzienda());
$azienda=$aziende[0];
?>



<div class="row">
    <div class="col-lg-12">
        <!-- Circle Buttons -->
        <div class="card shadow mb-4 border-left-primary">
            <div class="card-body">

                <h1 class="h3 mb-2 text-gray-800">Modifica Campo</h1>
                <p class="mb-4">In questa sezione verranno modificate le informazioni relative al nuovo campo. </p>


                <form action="index.php?link=3&id_campo=<?php echo $id_campo; ?>" method="post">
                    
                    <input type="hidden" class="form-control" name="latitudine" id="id-lat" value="<?php echo $campo->getLatitudine(); ?>">
                    <input type="hidden" class="form-control" name="longitudine" id="id-lng" value="<?php echo $campo->getLongitudine(); ?>">

                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="text">Luogo: *</label>
                                <input type="text" class="form-control" name="luogo" id="pac-input" value="<?php echo $campo->getLuogo();?>" required>
                            </div>
                        </div>
                        <div class="col-md-6"></div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="number">Dimensione in MQ: *</label>
                                <input type="text" class="form-control" name="dimensione" value="<?php echo $campo->getDimensione();?>" required>
                            </div>
                        </div>
                        <div class="col-md-6"></div>

                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="number">Azienda: </label>
                                <select id="id_azienda"  style="width: 100%;" class="form-control" name="id_azienda" tabindex="-1" aria-hidden="true" required>
                                    <option value="<?php echo $campo->getIdAzienda();?>"><?php echo $azienda->getRagioneSociale();?></option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-6"></div>

                        <div class="col-md-6">
                            <div id="map"></div>
                        </div>


                        <div class="col-md-12">
                            <div class="form-group">
                                <br />
                                <label for="number">I campi contrassegnati con * sono obbligatori per l'inserimento del campo.</label>
                            </div>
                        </div>
                    </div>
                    <button type="submit" class="btn btn-facebook" style="min-width: 250px" name="submitModificaCampo">Modifica</button>
                </form>
            </div>
        </div>
    </div>
</div>


