<?php

if(isset($_REQUEST['submitNuovoCampo'])){

    $dbMgr=new DbManager();
    $campiMgr = new CampiMgr($dbMgr);
    $dbMgr->startTransaction();
    $values['luogo'] = $_POST['luogo'];
    $values['latitudine'] = $_POST['latitudine'];
    $values['longitudine'] = $_POST['longitudine'];
    $values['dimensione'] = $_POST['dimensione'];
    $values['id_azienda'] = $_POST['id_azienda'];


    try{
        $campiMgr->insertCampo($values);
        $dbMgr->commit();
        header('Location: index.php?link=2');
        exit;
    } catch (Exception $e) {
        $dbMgr->rollback();
    }

}
?>


<div class="row">
    <div class="col-lg-12">
        <!-- Circle Buttons -->
        <div class="card shadow mb-4 border-left-primary">
            <div class="card-body">

                <h1 class="h3 mb-2 text-gray-800">Nuovo Campo</h1>
                <p class="mb-4">In questa sezione verranno inserite le informazioni relative al nuovo campo da creare. </p>


                <form action="#" method="post">

                    <input type="hidden" class="form-control" name="latitudine" id="id-lat" >
                    <input type="hidden" class="form-control" name="longitudine" id="id-lng" >
                    
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="text">Luogo: *</label>
                                <input type="text" class="form-control" name="luogo" id="pac-input" required onkeydown="showElement()">
                            </div>
                        </div>
                        <div class="col-md-6"></div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="number">Dimensione in MQ: *</label>
                                <input type="text" class="form-control" name="dimensione" required>
                            </div>
                        </div>
                        <div class="col-md-6"></div>

                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="number">Azienda: </label>
                                <select id="id_azienda"  style="width: 100%;" class="form-control" name="id_azienda" tabindex="-1" aria-hidden="true" required>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-6"></div>

                        <div class="col-md-6">
                            <div id="map" style="display:none"></div>
                        </div>
                        <div class="col-md-6"></div>


                        <div class="col-md-12">
                            <div class="form-group">
                                <br />
                                <label for="number">I campi contrassegnati con * sono obbligatori per l'inserimento del campo.</label>
                            </div>
                        </div>
                        
                    </div>
                    <button type="submit" class="btn btn-facebook" style="min-width: 250px" name="submitNuovoCampo">Inserisci</button>
                </form>
            </div>
        </div>
    </div>
</div>























