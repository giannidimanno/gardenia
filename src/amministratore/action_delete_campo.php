<?php
include 'inclusioni.php';
try{
    $dbMgr=new DbManager();
    $dbMgr->startTransaction();
    $campiMgr = new CampiMgr($dbMgr);
    $impiantiMgr = new ImpiantiMgr($dbMgr);


    $id=$_GET['id'];
    $campiMgr->deleteById($id);

    $impianti = $impiantiMgr->getImpiantiByIdCampo($id);
    foreach($impianti as $impianto){
        $impiantiMgr->deleteImpiantiById($impianto->getid());
    }


    $dbMgr->commit();
    header('Location: index.php?link=2');
    exit;
} catch (Exception $e) {
    $dbMgr->rollback();
    header('Location: logout.php');
    exit;
}


?>
