<?php

$Errore = FALSE;
$ip="https://mytunnel-alessandr1.pitunnel.com";
$url =$ip."/ApiRest/captureImage.php";
$image="img/placeholder.jpg";

if(isset($_REQUEST['submit']))
{
    try{
       //open connection
        $ch = curl_init();

        //set the url, number of POST vars, POST data
        curl_setopt($ch,CURLOPT_URL, $url);
        curl_setopt($ch,CURLOPT_POST, true);

        //So that curl_exec returns the contents of the cURL; rather than echoing it
        curl_setopt($ch,CURLOPT_RETURNTRANSFER, true);

        //execute post
        $result = curl_exec($ch);
        $image=$ip."/image/".$result;

    }catch(Exception $e){
        $Errore = TRUE;
    }
    if($Errore == TRUE){
        echo "Errore";
    }
}

?>



<div class="row">
    <div class="col-lg-12">
        <!-- Circle Buttons -->
        <div class="card shadow mb-4 border-left-primary">
            <div class="card-body">

                <h1 class="h3 mb-2 text-gray-800">Acquisizione Foto</h1>
                <p class="mb-4">In questa sezione verrà visualizzata la foto acquisita in real time. </p>


                <form method="post" action="index.php?link=8">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <img src="<?php echo $image; ?>"  class="form-group" style="max-width: 60%">
                            </div>
                        </div>
                    </div>
                    <button type="submit" class="btn btn-facebook" style="min-width: 250px" name="submit">Acquisisci</button>
                </form>
            </div>
        </div>
    </div>
</div>

