<?php

$errore = FALSE;
$erroreEmpty = FALSE;
$erroreInUso = FALSE;
$errorePassword = FALSE;

$dbMgr = new DbManager();
$dbMgr->startTransaction();
$utentiMgr = new UtentiMgr($dbMgr);
$idUtente=$_GET['id_utente'];

if(isset($_REQUEST['submitModificaUtente'])) {

    try {

        $values['nome'] = $_POST['nome'];
        $values['cognome'] = $_POST['cognome'];
        $values['email'] = $_POST['email'];
        $values['password'] = $_POST['password'];
        $values['amministratore'] = $_POST['amministratore'];
        $values['telefono'] = $_POST['telefono'];

        $idUtenteCreato = $utentiMgr->updateUtenti($idUtente,$values);
        $dbMgr->commit();
        header('Location: index.php?link=17');

    } catch (Exception $e) {
        $dbMgr->rollback();
        $errore = TRUE;
    }

}

$utenti=$utentiMgr->getUtentiById($idUtente);
$utente=$utenti[0];

?>

<?php include 'head.php'; ?>
<html>
<body>
<div class="row">
    <div class="col-lg-12">
        <!-- Circle Buttons -->
        <div class="card shadow mb-4 border-left-primary">
            <div class="card-body">

            <h1 class="h3 mb-2 text-gray-800">Nuovo Utente</h1>
            <p class="mb-4">In questa sezione verranno modificate le informazioni relative al nuovo Utente/Amministratore. </p>


            <form action="index.php?link=18&id_utente=<?php echo $idUtente;?>" method="post">
                <div class="col-md-6"></div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="number">Nome</label>
                        <input type="text" class="form-control" name="nome" value="<?php echo $utente->getNome();?>" required>
                    </div>
                </div>
                <div class="col-md-6"></div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="number">Cognome</label>
                        <input type="text" class="form-control" name="cognome" value="<?php echo $utente->getCognome();?>" required>
                    </div>
                </div>
                <div class="col-md-6"></div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="number">Idirizzo Email</label>
                        <input type="text" class="form-control" name="email" value="<?php echo $utente->getEmail();?>" required readonly>
                    </div>
                </div>
                <div class="col-md-6"></div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="number">Password</label>
                        <input type="text" class="form-control" name="password" value="<?php echo $utente->getPassword();?>" required>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="number">Amministratore: *</label>
                        <select name="amministratore" id="amministratore" class="form-control">
                            <?php
                                if($utente->getAmministratore()=="si"){
                                    echo '
                                    <option value="si" selected>SI</option>
                                    <option value="no">NO</option>
                                    ';
                                }else{
                                    echo '
                                    <option value="si">SI</option>
                                    <option value="no" selected>NO</option>
                                    ';
                                }
                            ?>
                        </select>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="number">Telefono</label>
                        <input type="text" class="form-control" name="telefono" value="<?php echo $utente->getTelefono();?>" required>
                    </div>
                </div>

                    <button type="submit" class="btn btn-facebook" style="min-width: 250px" name="submitModificaUtente">Modifica</button>
                <div class="text-center">
                        <?php
                        if($errore == TRUE){
                            echo "Errore nell'inserimento";
                            echo "<br><br>";
                        }else if($erroreInUso == TRUE){
                            echo "Email già presente a sistema";
                            echo "<br><br>";
                        }
                        ?>
                </div>
            </form>
        </div>
    </div>
</div>
</div>
</body>
</html>