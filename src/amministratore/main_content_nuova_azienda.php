<?php

if(isset($_REQUEST['submitNuovaAzienda'])){

    $dbMgr=new DbManager();
    $utentiMgr = new UtentiMgr($dbMgr);
    $aziendeMgr = new AziendeMgr($dbMgr);


    $dbMgr->startTransaction();
    $values['ragione_sociale'] = $_POST['ragione_sociale'];
    $values['indirizzo'] = $_POST['indirizzo'];
    $values['telefono'] = $_POST['telefono'];
    $values['codice_fiscale'] = $_POST['codice_fiscale'];
    $values['partita_iva'] = $_POST['partita_iva'];
    $values['id_utente'] = $_POST['id_utente'];


    try{
        $aziendeMgr->insertAzienda($values);
        $dbMgr->commit();
        header('Location: index.php?link=12');
        exit;
    } catch (Exception $e) {
        $dbMgr->rollback();
    }
}


?>



<div class="row">
    <div class="col-lg-12">
        <!-- Circle Buttons -->
        <div class="card shadow mb-4 border-left-primary">
            <div class="card-body">

                <h1 class="h3 mb-2 text-gray-800">Nuova Azienda</h1>
                <p class="mb-4">In questa sezione verranno inserite le informazioni relative alla nuova azienda da creare. </p>


                <form action="#" method="post">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="text">Ragione Sociale: *</label>
                                <input type="text" class="form-control" name="ragione_sociale" required>
                            </div>
                        </div>
                        <div class="col-md-6"></div>

                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="number">Indirizzo: *</label>
                                <input type="text" class="form-control" name="indirizzo" required>
                            </div>
                        </div>
                        <div class="col-md-6"></div>

                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="number">Telefono: *</label>
                                <input type="text" class="form-control" name="telefono" required>
                            </div>
                        </div>
                        <div class="col-md-6"></div>

                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="number">Codice fiscale: </label>
                                <input type="text" class="form-control" name="codice_fiscale">
                            </div>
                        </div>
                        <div class="col-md-6"></div>

                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="number">Partita Iva: </label>
                                <input type="text" class="form-control" name="partita_iva">
                            </div>
                        </div>
                        <div class="col-md-6"></div>


                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="number">Utente: </label>
                                <select id="id_utente"  style="width: 100%;" class="form-control" name="id_utente" tabindex="-1" aria-hidden="true" required>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-6"></div>


                        <div class="col-md-12">
                            <div class="form-group">
                                <br />
                                <label for="number">I campi contrassegnati con * sono obbligatori per l'inserimento dell'azienda.</label> <br>
                                <label for="number">E' obbligatorio inserire il Codice Fiscale o la Partita Iva.</label>
                            </div>
                        </div>
                    </div>
                    <button type="submit" class="btn btn-facebook" style="min-width: 250px" name="submitNuovaAzienda">Inserisci</button>
                </form>
            </div>
        </div>
    </div>
</div>





















