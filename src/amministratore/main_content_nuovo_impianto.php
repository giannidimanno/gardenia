<?php
$dbMgr=new DbManager();
$impiantiMgr = new impiantiMgr($dbMgr);


if(isset($_REQUEST['submitNuovoImpianto'])) {
    $dbMgr=new DbManager();
    $dbMgr->startTransaction();

    $values['nome'] = $_POST['nome'];
    $values['dimensioni'] = $_POST['dimensioni'];
    $values['stato'] = $_POST['stato'];
    $values['id_campo'] = $_POST['id_campo'];
    $values['device'] = $_POST['device'];

    try{
        $impiantiMgr->insertImpianto($values);
        $dbMgr->commit();
        header('location:index.php?link=6');
    }catch(Exception $e){
        var_dump($e->getMessage());die();
        $dbMgr->rollback();
    }
}


?>

<div class="row">
    <div class="col-lg-12">
        <!-- Circle Buttons -->
        <div class="card shadow mb-4 border-left-primary">
            <div class="card-body">

                <h1 class="h3 mb-2 text-gray-800">Nuovo impianto</h1>
                <p class="mb-4">In questa sezione verranno inserite le informazioni relative al nuovo impianto da creare. </p>


                <form action="#" method="post">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="number">Campo: *</label>
                                <select id="id_campo"  style="width: 100%;" class="form-control" name="id_campo" tabindex="-1" aria-hidden="true" required>
                                </select>                               
                            </div>
                        </div>
                        <div class="col-md-6"></div>

                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="text">Nome: *</label>
                                <input type="text" class="form-control" name="nome" required>
                            </div>
                        </div>
                        <div class="col-md-6"></div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="number">Dimensione in MQ: *</label>
                                <input type="text" class="form-control" name="dimensioni" required>
                            </div>
                        </div>
                        <div class="col-md-6"></div>

                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="number">Seriale Dispositivo: *</label>
                                <input type="text" class="form-control" name="device" required>

                            </div>
                        </div>
                        <div class="col-md-6"></div>

                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="number">Stato: *</label>
                                <select name="stato" id="stato" class="form-control">
                                    <option value="1">Si</option>
                                    <option value="0">No</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-6"></div>



                        <div class="col-md-12">
                            <div class="form-group">
                                <br />
                                <label for="number">I campi contrassegnati con * sono obbligatori per l'inserimento del campo.</label>
                            </div>
                        </div>
                    </div>
                    <button type="submit" class="btn btn-facebook" style="min-width: 250px" name="submitNuovoImpianto">Inserisci</button>
                </form>
            </div>
        </div>
    </div>
</div>

