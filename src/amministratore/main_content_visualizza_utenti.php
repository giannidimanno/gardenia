
<?php

$dbMgr = new DbManager();
$utentiMgr = new UtentiMgr($dbMgr);

try{
    $utenti = $utentiMgr->getAllUtentiNotAdmin();
} catch(Exception $e){
    $utenti = [];
}

?>

<div class="row">
    <div class="col-lg-12">
        <!-- Circle Buttons -->
        <div class="card shadow mb-4 border-left-primary">
            <div class="card-body" style="overflow-x: auto">

                <h1 class="h3 mb-2 text-gray-800">Visualizza utenti</h1>
                <p class="mb-4">In questa sezione è possibile visualizzare tutti gli utenti.</p>
                <form action="index.php?" method="get">

                </form>

                <table class="table table-bordered" width="100%" cellspacing="0">
                    <thead>
                    <tr>
                        <th>Id</th>
                        <th style="min-width:200px;">Nome</th>
                        <th>Cognome</th>
                        <th>Indirizzo Email</th>
                        <th>Password</th>
                        <th>Telefono</th>
                        <th>Amministratore</th>
                        <th style="min-width:130px;width: 130px">Action</th>
                    </tr>
                    </thead>

                    <tbody>

                    <?php

                    foreach($utenti as $utente){



                        echo '
                            <tr>
                                <td>'.$utente->getId().'</td>
                                <td>'.$utente->getNome().'</td>
                                <td>'.$utente->getCognome().'</td>
                                <td>'.$utente->getEmail().'</td>
                                <td>'.$utente->getPassword().'</td>
                                <td>'.$utente->getTelefono().'</td>
                                <td>'.$utente->getAmministratore().'</td>
                                <td>
                                <a href="index.php?link=18&id_utente='.$utente->getId().'" class="btn btn-primary btn-circle btn-sm">
                                    <i class="fas fa-pencil-alt"></i>
                                </a>
                                <a data-toggle="modal" data-target="#modaleUtente" href="#" data-id="'.$utente->getId().'" class="btn btn-danger btn-circle btn-sm">                             
                                <i class="fas fa-trash"></i>                           
                             </a>
                            </td>
                        </tr>
                       ';
                    }

                    ?>
                    </tbody>
                </table>
                <br>
            </div>
        </div>
    </div>
</div>
