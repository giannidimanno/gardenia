<?php

$dbMgr=new DbManager();
$aziendeMgr = new AziendeMgr($dbMgr);
$UtenteMgr = new UtentiMgr($dbMgr);

$azienda_id=$_GET['azienda_id'];

if(isset($_REQUEST['submitModificaAzienda'])){

    $values['ragione_sociale'] = $_POST['ragione_sociale'];
    $values['indirizzo'] = $_POST['indirizzo'];
    $values['telefono'] = $_POST['telefono'];
    $values['codice_fiscale'] = $_POST['codice_fiscale'];
    $values['partita_iva'] = $_POST['partita_iva'];
    $values['id_utente'] = $_POST['id_utente'];

    try{
        $dbMgr->startTransaction();

        $aziendeMgr->updateAziendaById($azienda_id,$values);
        $dbMgr->commit();
        header('Location: index.php?link=12');
        exit;
    } catch (Exception $e) {
        $dbMgr->rollback();
    }

}

$aziende=$aziendeMgr->getAziendaById($azienda_id);
$azienda=$aziende[0];


$utenti=$UtenteMgr->getUtentiById($azienda->getIdUtente());
$utente=$utenti[0];

?>



<div class="row">
    <div class="col-lg-12">
        <!-- Circle Buttons -->
        <div class="card shadow mb-4 border-left-primary">
            <div class="card-body">

                <h1 class="h3 mb-2 text-gray-800">Modifica Azienda</h1>
                <p class="mb-4">In questa sezione verranno modificate le informazioni relative all'azienda. </p>


                <form action="index.php?link=13&azienda_id=<?php echo $azienda_id; ?>" method="post">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="text">Ragione Sociale: *</label>
                                <input type="text" class="form-control" name="ragione_sociale" value="<?php echo $azienda->getRagioneSociale();?>" required>
                            </div>
                        </div>
                        <div class="col-md-6"></div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="number">Indirizzo: *</label>
                                <input type="text" class="form-control" name="indirizzo" value="<?php echo $azienda->getIndirizzo();?>" required>
                            </div>
                        </div>
                        <div class="col-md-6"></div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="number">Telefono: *</label>
                                <input type="text" class="form-control" name="telefono" value="<?php echo $azienda->getTelefono();?>" required>
                            </div>
                        </div>
                        <div class="col-md-6"></div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="number">Codice Fiscale: </label>
                                <input type="text" class="form-control" name="codice_fiscale" value="<?php echo $azienda->getCodiceFiscale();?>">
                            </div>
                        </div>

                        <div class="col-md-6"></div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="number">Partita Iva: </label>
                                <input type="text" class="form-control" name="partita_iva" value="<?php echo $azienda->getPartitaIva();?>">
                            </div>
                        </div>
                        <div class="col-md-6"></div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="number">Utente: </label>
                                <select id="id_utente"  style="width: 100%;" class="form-control" name="id_utente" tabindex="-1" aria-hidden="true" required>
                                    <option value="<?php echo $azienda->getIdUtente();?>"><?php echo $utente->getCognome().' '.$utente->getNome();?></option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-6"></div>

                        <div class="col-md-12">
                            <div class="form-group">
                                <br />
                                <label for="number">I campi contrassegnati con * sono obbligatori per la modifica dell'azienda.</label>
                            </div>
                        </div>
                    </div>
                    <button type="submit" class="btn btn-facebook" style="min-width: 250px" name="submitModificaAzienda">Modifica</button>
                </form>
            </div>
        </div>
    </div>
</div>
