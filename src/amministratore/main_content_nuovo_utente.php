<?php



$errore = FALSE;
$erroreEmpty = FALSE;
$erroreInUso = FALSE;
$errorePassword = FALSE;

if(isset($_REQUEST['submitNuovoUtente'])) {

    try {
        $dbMgr = new DbManager();
        $dbMgr->startTransaction();
        $utentiMgr = new UtentiMgr($dbMgr);


        if ($utentiMgr->checkEmailExist($_POST['email']) == true) {
            $dbMgr->rollback();
            $erroreInUso = TRUE;


        } else {

            $values['nome'] = $_POST['nome'];
            $values['cognome'] = $_POST['cognome'];
            $values['email'] = $_POST['email'];
            $values['password'] = $_POST['password'];
            $values['amministratore'] = $_POST['amministratore'];
            $values['telefono'] = $_POST['telefono'];

            $idUtenteCreato = $utentiMgr->insertUtenti($values);
            $dbMgr->commit();
            header('Location: index.php?link=17');
        }
    } catch (Exception $e) {
        $dbMgr->rollback();
        $errore = TRUE;
    }

}
?>

<?php include 'head.php'; ?>
<html>
<body>
<div class="row">
    <div class="col-lg-12">
        <!-- Circle Buttons -->
        <div class="card shadow mb-4 border-left-primary">
            <div class="card-body">

            <h1 class="h3 mb-2 text-gray-800">Nuovo Utente</h1>
            <p class="mb-4">In questa sezione verranno inserite le informazioni relative al nuovo Utente/Amministratore. </p>


            <form action="index.php?link=16" method="post">
                <div class="col-md-6"></div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="number">Nome</label>
                        <input type="text" class="form-control" name="nome" required>
                    </div>
                </div>
                <div class="col-md-6"></div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="number">Cognome</label>
                        <input type="text" class="form-control" name="cognome" required>
                    </div>
                </div>
                <div class="col-md-6"></div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="number">Idirizzo Email</label>
                        <input type="text" class="form-control" name="email" required>
                    </div>
                </div>
                <div class="col-md-6"></div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="number">Password</label>
                        <input type="text" class="form-control" name="password" required>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="number">Amministratore: *</label>
                        <select name="amministratore" id="amministratore" class="form-control">
                            <option value="si">SI</option>
                            <option value="no">NO</option>
                        </select>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="number">Telefono</label>
                        <input type="text" class="form-control" name="telefono" required>
                    </div>
                </div>

                <?php
                if($errore == TRUE){
                    echo "Errore nell'inserimento";
                }else if($erroreInUso == TRUE){
                    echo "<p style='color: red'>Email già presente a sistema</p>";
                }
                ?>

                <button type="submit" class="btn btn-facebook" style="min-width: 250px" name="submitNuovoUtente">Inserisci</button>

            </form>
        </div>
    </div>
</div>
</div>
</body>
</html>