<?php

$dbMgr=new DbManager();
$impiantiMgr = new impiantiMgr($dbMgr);
$campiMgr = new CampiMgr($dbMgr);
$aziendeMgr = new AziendeMgr($dbMgr);

try{
    $campi=$campiMgr->getAllCampi();
}catch(Exception $e){
    $campi=[];
}


?>





<div class="row">
    <div class="col-lg-12">
        <!-- Circle Buttons -->
        <div class="card shadow mb-4 border-left-primary">
            <div class="card-body" style="overflow-x: auto">

                <h1 class="h3 mb-2 text-gray-800">Visualizza Campi</h1>
                <p class="mb-4">In questa sezione è possibile visualizzare tutti i tuoi campi.</p>
                <form action="index.php?" method="get">

                </form>

                <table class="table table-bordered" width="100%" cellspacing="0">
                    <thead>
                    <tr>
                        <th>Id</th>
                        <th style="min-width:200px;">Luogo</th>
                        <th>Latitudine</th>
                        <th>Longitudine</th>
                        <th>Dimensioni</th>
                        <th>Azienda</th>
                        <th style="min-width:130px;width: 130px">Action</th>
                    </tr>
                    </thead>

                    <tbody>



                    <?php

                    foreach($campi as $campo){
                        $aziende = $campo->getIdAzienda();
                        $azienda = $aziende[0];
                        $infoAzienda = $aziendeMgr->getAziendaById($azienda);
                        $ragioneSociale = $infoAzienda[0]->getRagioneSociale();

                        echo '
                        <tr>
                            <td>'.$campo->getId().'</td>
                            <td>'.$campo->getLuogo().'</td>
                            <td>'.$campo->getLatitudine().'</td>
                            <td>'.$campo->getLongitudine().'</td>
                            <td>'.$campo->getDimensione().'</td>
                            <td>'.$ragioneSociale.'</td>
                            <td>
                                <a href="index.php?link=3&id_campo='.$campo->getId().'" class="btn btn-primary btn-circle btn-sm">
                                    <i class="fas fa-pencil-alt"></i>
                                </a>
                                <a data-toggle="modal" data-target="#modaleCampo" href="#" data-id="'.$campo->getId().'" class="btn btn-danger btn-circle btn-sm">                             
                                    <i class="fas fa-trash"></i>
                                    
                                </a>
                            </td>
                    </tr>
                        ';
                    }
                    ?>
                    </tbody>
                </table>
                <br />



            </div>
        </div>
    </div>
</div>
