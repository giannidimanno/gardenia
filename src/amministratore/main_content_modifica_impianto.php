<?php

$id_impianto=$_GET['id_impianto'];
$dbMgr=new DbManager();
$impiantiMgr = new impiantiMgr($dbMgr);
$campiMgr = new CampiMgr($dbMgr);
$aziendaMgr = new AziendeMgr($dbMgr);

try{
    $campi=$campiMgr->getAllCampi();
}catch(Exception $e){
    $campi=[];
}



// MODIFICA impianto
if(isset($_REQUEST['submitModificaimpianto'])) {
    $dbMgr=new DbManager();
    $dbMgr->startTransaction();

    $values['nome'] = $_POST['nome'];
    $values['dimensioni'] = $_POST['dimensioni'];
    $values['stato'] = $_POST['stato'];
    $values['id_campo'] = $_POST['id_campo'];
    $values['device'] = $_POST['device'];
    
    try{
        $impiantiMgr->updateImpiantoById($values, $id_impianto);

        $dbMgr->commit();
        header('location:index.php?link=6');
    }catch(Exception $e){
        $dbMgr->rollback();
    }

}

$impianti=$impiantiMgr->getImpiantiById($id_impianto);
$impianto=$impianti[0];

$campi=$campiMgr->getCampoById($impianto->getIdCampo());
$campo=$campi[0];

$aziende=$aziendaMgr->getAziendaById($campo->getIdAzienda());
$azienda=$aziende[0];

?>

<div class="row">
    <div class="col-lg-12">
        <!-- Circle Buttons -->
        <div class="card shadow mb-4 border-left-primary">
            <div class="card-body">

                <h1 class="h3 mb-2 text-gray-800">Modifica impianto</h1>
                <p class="mb-4">In questa sezione puoi modificare le informazioni relative alla impianto selezionata</p>


                <form action="index.php?link=7&id_impianto=<?php echo $id_impianto;?>" method="post">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="number">Campo: *</label>
                                <select id="id_campo"  style="width: 100%;" class="form-control" name="id_campo" tabindex="-1" aria-hidden="true" required>
                                    <option value="<?php echo $impianto->getIdCampo();?>"><?php echo $azienda->getRagioneSociale().' - '.$campo->getLuogo();?></option>
                                </select>                               
                            </div>
                        </div>
                        <div class="col-md-6"></div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="text">Nome: *</label>
                                <input type="text" class="form-control" name="nome" required value="<?php echo $impianto->getNome() ?>">
                            </div>
                        </div>
                        <div class="col-md-6"></div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="number">Dimensione in MQ: *</label>
                                <input type="text" class="form-control" name="dimensioni" required value="<?php echo $impianto->getDimensioni() ?>">
                            </div>
                        </div>
                        <div class="col-md-6"></div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="number">Dispositivo utilizzato: *</label>
                                <input type="text" class="form-control" name="dimensioni" required value="<?php echo $impianto->getDevice() ?>">
                            </div>
                        </div>
                        <div class="col-md-6"></div>

                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="number">Stato: *</label>
                                <select name="stato" id="stato" class="form-control" value="<?php echo $impianto->getStato() ?>">
                                    <option value="1">Si</option>
                                    <option value="0">No</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-6"></div>



                        <div class="col-md-12">
                            <div class="form-group">
                                <br />
                                <label for="number">I campi contrassegnati con * sono obbligatori per l'aggiornamento della impianto.</label>
                            </div>
                        </div>
                    </div>
                    <button type="submit" class="btn btn-facebook" style="min-width: 250px" name="submitModificaimpianto">Modifica</button>
                </form>
            </div>
        </div>
    </div>
</div>


