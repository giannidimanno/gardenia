<?php
$dbMgr=new DbManager();

$impiantiMgr = new impiantiMgr($dbMgr);
$campiMgr = new CampiMgr($dbMgr);
$aziendaMgr = new AziendeMgr($dbMgr);

try{
    $impianti = $impiantiMgr->getAllImpianti();
}catch(Exception $e){
    $impianti=[];
}


?>

<div class="row">
    <div class="col-lg-12">
        <!-- Circle Buttons -->
        <div class="card shadow mb-4 border-left-primary">
            <div class="card-body" style="overflow-x: auto">

                <h1 class="h3 mb-2 text-gray-800">Visualizza impianti</h1>
                <p class="mb-4">In questa sezione è possibile visualizzare tutti gli impianti.</p>
                <form action="index.php?" method="get">

                </form>

                <table class="table table-bordered" width="100%" cellspacing="0">
                    <thead>
                    <tr>
                        <th>Id</th>
                        <th style="min-width:200px;">Nome</th>
                        <th>Azienda</th>
                        <th>Luogo Campo</th>
                        <th>Stato</th>
                        <th style="min-width:130px;width: 130px">Action</th>
                    </tr>
                    </thead>

                    <tbody>



                    <?php

                    foreach($impianti as $impianto){
                        $campi = $campiMgr->getCampoById($impianto->getIdCampo());
                        $campo=$campi[0];

                        $aziende=$aziendaMgr->getAziendaById($campo->getIdAzienda());
                        $azienda=$aziende[0];

                        $luogo = $campo->getLuogo();
                        $stato="Disattiva";
                        if($impianto->getStato()==1){
                            $stato="Attivo";
                        }

                        echo '
                        <tr>
                            <td>'.$impianto->getId().'</td>
                            <td>'.$impianto->getNome().'</td>
                            <td>'.$azienda->getRagioneSociale().'</td>
                            <td>'.$luogo.'</td>
                            <td>'.$stato.'</td>

  
                            <td>
                                <a href="index.php?link=7&id_impianto='.$impianto->getId().'" class="btn btn-primary btn-circle btn-sm">
                                    <i class="fas fa-pencil-alt"></i>
                                </a>
                                <a data-toggle="modal" data-target="#modaleimpianto" href="#" data-id="'.$impianto->getId().'" class="btn btn-danger btn-circle btn-sm">                             
                                    <i class="fas fa-trash"></i>
                                    
                                </a>
                            </td>
                    </tr>
                        ';


                    }



                    ?>


                    </tbody>
                </table>
                <br />
            </div>
        </div>
    </div>
</div>
