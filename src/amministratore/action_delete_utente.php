<?php
include 'inclusioni.php';
try{
    $dbMgr=new DbManager();
    $dbMgr->startTransaction();
    $utentiMgr = new UtentiMgr($dbMgr);
    $aziendeMgr = new AziendeMgr($dbMgr);
    $campiMgr = new CampiMgr($dbMgr);
    $impiantiMgr = new ImpiantiMgr($dbMgr);

    $id=$_GET['id'];
    $utentiMgr->deleteUtenti($id);

    $aziende = $aziendeMgr->getAziendaByIdUtente($id);
    foreach($aziende as $azienda){
        $aziendeMgr->deleteAziendaById($azienda->getid());
    }

    $campi=$campiMgr->getCampiByIdUtente($id);
    foreach($campi as $campo){
        $campiMgr->deleteById($campo->getid());
    }

    $impianti=$impiantiMgr->getImpiantiByIdUtente($id);
    foreach($impianti as $impianto){
        $impiantiMgr->deleteImpiantiById($impianto->getid());
    }


    $dbMgr->commit();
    header('Location: index.php?link=17');
    exit;
} catch (Exception $e) {
    $dbMgr->rollback();
    var_dump($e->getMessage());
    //header('Location: logout.php');
    exit;
}


?>

