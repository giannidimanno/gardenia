<?php
$rooting=$_GET['link'];

?>

<!-- Sidebar -->
<ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">

  <!-- Sidebar - Brand -->
  <a class="sidebar-brand d-flex align-items-center justify-content-center" href="index.php?link=0">
	<div class="sidebar-brand-icon ">
	  <!--<i class="fas fa-laugh-wink"></i>-->
	  <img src="../img/undraw_profile_2.svg" width="60px" />
	</div>
	<div class="sidebar-brand-text mx-8"> <sup>ADMIN AATECH</sup></div>
  </a>



  <!-- Divider -->
  <hr class="sidebar-divider my-0">

  <!-- Nav Item - Dashboard -->
  <?php
    if($rooting==0)  {
       echo '<li class="nav-item active">';
    }else{
        echo '<li class="nav-item">';
    }
    ?>
	<a class="nav-link" href="index.php?link=0">
	  <i class="fas fa-fw fa-tachometer-alt"></i>
	  <span>Dashboard</span></a>

<?php
if( in_array($rooting,[16, 17, 18])){
    echo '<li class="nav-item active">';
}else{
    echo '<li class="nav-item">';
}
?>
<a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseUtente" aria-expanded="true" aria-controls="collapseUtente">
    <i class="fas fa-fw fa-users"></i>
    <span>Utente</span>
</a>
<div id="collapseUtente" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionSidebar">
    <div class="bg-white py-2 collapse-inner rounded">
        <h6 class="collapse-header">Scegli:</h6>
        <a class="collapse-item" href="index.php?link=16">Nuovo Utente</a>
        <a class="collapse-item" href="index.php?link=17">Visualizza Utente</a>
    </div>
</div>
</li>

    <!-- Divider -->
    <hr class="sidebar-divider my-0">
<?php
if( in_array($rooting,[11, 12, 13])){
    echo '<li class="nav-item active">';
}else{
    echo '<li class="nav-item">';
}
?>
<a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseAzienda" aria-expanded="true" aria-controls="collapseAzienda">
<i class="fas fa-fw fa-briefcase"></i>
<span>Azienda</span>
</a>
<div id="collapseAzienda" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionSidebar">
<div class="bg-white py-2 collapse-inner rounded">
  <h6 class="collapse-header">Scegli:</h6>
  <a class="collapse-item" href="index.php?link=11">Nuova Azienda</a>
  <a class="collapse-item" href="index.php?link=12">Visualizza Aziende</a>
</div>
</div>
</li>

<!-- -----FINE AZIENDA -->




	  
  <!-- Divider -->
  <hr class="sidebar-divider my-0">

    <?php
    if( in_array($rooting,[1,2,3,4])){
        echo '<li class="nav-item active">';
    }else{
        echo '<li class="nav-item">';
    }
    ?>
	<a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseCampo" aria-expanded="true" aria-controls="collapseCampo">
	  <i class="fas fa-fw fa-leaf"></i>
	  <span>Gestione Campo</span>
	</a>
	<div id="collapseCampo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionSidebar">
	  <div class="bg-white py-2 collapse-inner rounded">
		  <h6 class="collapse-header">Scegli:</h6>
		  <a class="collapse-item" href="index.php?link=1">Nuovo Campo</a>
		  <a class="collapse-item" href="index.php?link=2&page=1">Visualizza Campi</a>
	  </div>
	</div>
  </li>


    <!-- Divider -->
    <hr class="sidebar-divider my-0">

<?php
if( in_array($rooting,[5,6,18])){
    echo '<li class="nav-item active">';
}else{
    echo '<li class="nav-item">';
}
?>
<a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseimpianto" aria-expanded="true" aria-controls="collapseimpianto">
<i class="fab fa-fw fa-accusoft"></i>
<span>Gestione Impianti</span>
</a>
<div id="collapseimpianto" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionSidebar">
<div class="bg-white py-2 collapse-inner rounded">
  <h6 class="collapse-header">Scegli:</h6>
  <a class="collapse-item" href="index.php?link=5">Nuovo Impianto </a>
  <a class="collapse-item" href="index.php?link=6&page=1">Visualizza Impianti</a>
</div>
</div>
</li>
  






  






    <!-- Divider -->
    <hr class="sidebar-divider my-0">





    <!-- Divider -->
  <hr class="sidebar-divider my-0">	  
  
   <!-- Nav Item - Charts -->
  <li class="nav-item">
	<a class="nav-link" href="#" data-toggle="modal" data-target="#logoutModal">
	  <i class="fas fa-sign-out-alt fa-sm fa-fw mr-2"></i>
	  <span>Logout</span>
	</a>
  </li>
  


  <!-- Divider -->
  <hr class="sidebar-divider d-none d-md-block">

  <!-- Sidebar Toggler (Sidebar) -->
  <div class="text-center d-none d-md-inline">
	<button class="rounded-circle border-0" id="sidebarToggle"></button>
  </div>

</ul>
<!-- End of Sidebar -->