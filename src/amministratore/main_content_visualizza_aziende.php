<?php

$dbMgr = new DbManager();
$aziendeMgr = new AziendeMgr($dbMgr);
$utentiMgr = new UtentiMgr($dbMgr);


try{
    $aziende = $aziendeMgr->getAllAziende();
} catch(Exception $e){
    $aziende = [];
}

?>

<div class="row">
    <div class="col-lg-12">
        <!-- Circle Buttons -->
        <div class="card shadow mb-4 border-left-primary">
            <div class="card-body" style="overflow-x: auto">

                <h1 class="h3 mb-2 text-gray-800">Visualizza Aziende</h1>
                <p class="mb-4">In questa sezione è possibile visualizzare tutte le aziende.</p>
                <form action="index.php?" method="get">

                </form>

                <table class="table table-bordered" width="100%" cellspacing="0">
                    <thead>
                    <tr>
                        <th>Id</th>
                        <th style="min-width:200px;">Ragione Sociale</th>
                        <th>Indirizzo</th>
                        <th>Telefono</th>
                        <th>Codice Fiscale</th>
                        <th>Partita Iva</th>
                        <th>Utente</th>
                        <th style="min-width:130px;width: 130px">Action</th>
                    </tr>
                    </thead>

                    <tbody>



                    <?php

                    foreach($aziende as $azienda){

                        $utenti = $azienda->getIdUtente();
                        $utente = $utenti[0];
                        $infoUtente = $utentiMgr->getUtentiById($utente);
                        $nome = $infoUtente[0]->getNome().' '.$infoUtente[0]->getCognome();

                        echo '
                            <tr>
                                <td>'.$azienda->getId().'</td>
                                <td>'.$azienda->getRagioneSociale().'</td>
                                <td>'.$azienda->getIndirizzo().'</td>
                                <td>'.$azienda->getTelefono().'</td>
                                <td>'.$azienda->getCodiceFiscale().'</td>
                                <td>'.$azienda->getPartitaIva().'</td>
                                <td>'.$nome.'</td>
                                <td>
                                <a href="index.php?link=13&azienda_id='.$azienda->getId().'" class="btn btn-primary btn-circle btn-sm">
                                    <i class="fas fa-pencil-alt"></i>
                                </a>
                                <a data-toggle="modal" data-target="#modaleAzienda" href="#" data-id="'.$azienda->getId().'" class="btn btn-danger btn-circle btn-sm">                             
                                <i class="fas fa-trash"></i>
                                
                            </a>
    
                            </td>
    
                        </tr>
                            ';
                    }
                    ?>



                    </tbody>
                </table>
                <br />



            </div>
        </div>
    </div>
</div>


