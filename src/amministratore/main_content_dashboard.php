<?php

$dbMgr=new DbManager();
$campiMgr = new CampiMgr($dbMgr);
$utentiMgr = new UtentiMgr($dbMgr);

try{
    $totaleUtenti=count($utentiMgr->getAllUtentiNotAdmin());
}catch (Exception $e){
    $totaleUtenti=0;
}

try{
    $totaleCampi=count($campiMgr->getAllCampi());
}catch (Exception $e){
    $totaleCampi=0;
}




?>

<!-- Page Heading -->
<div class="d-sm-flex align-items-center justify-content-between mb-4">
<h1 class="h3 mb-0 text-gray-800">Dashboard</h1>
</div>

<!-- Content Row -->
<div class="row">

    <!-- Earnings (Monthly) Card Example -->
    <div class="col-xl-4 col-md-4 mb-4">
      <div class="card border-left-warning shadow h-100 py-2">
        <div class="card-body">
          <div class="row no-gutters align-items-center">
            <div class="col mr-2">
              <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">DATA</div>
              <div class="h5 mb-0 font-weight-bold text-gray-800"><?php echo date("d/m/y"); ?></div>
            </div>
            <div class="col-auto">
              <i class="fas fa-calendar fa-2x text-gray-300"></i>
            </div>
          </div>
        </div>
      </div>
    </div>

    <div class="col-xl-4 col-md-4 mb-4">
        <div class="card border-left-primary shadow h-100 py-2">
            <div class="card-body">
                <div class="row no-gutters align-items-center">
                    <div class="col mr-2">
                        <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">TOTALE UTENTI</div>
                        <div class="h5 mb-0 font-weight-bold text-gray-800"><?php echo $totaleUtenti; ?></div>
                    </div>
                    <div class="col-auto">
                        <i class="fas fa-users fa-2x text-gray-300"></i>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Pending Requests Card Example -->
    <div class="col-xl-4 col-md-4 mb-4">
      <div class="card border-left-info shadow h-100 py-2">
        <div class="card-body">
          <div class="row no-gutters align-items-center">
            <div class="col mr-2">
              <div class="text-xs font-weight-bold text-warning text-uppercase mb-1">TOTALE CAMPI</div>
              <div class="h5 mb-0 font-weight-bold text-gray-800"><?php echo $totaleCampi; ?></div>
            </div>
            <div class="col-auto">
              <i class="fas fa-leaf fa-2x text-gray-300"></i>
            </div>
          </div>
        </div>
      </div>
    </div>



</div>

<hr>
<br />

<div class="row">

        <div class="col-lg-6 mb-4">
            <a href="index.php?link=17&page=1" style="text-decoration:none;">
                <div class="card bg-primary text-white shadow" style="min-height: 120px;">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-2" align="center" style="margin-bottom: 5px">
                                <i class="fas fa-users fa-5x text-white-300"></i>
                            </div>
                            <div class="col-md-10">
                                <h5>Gestione Utenti</h5>
                                <div class="text-white-50 small"><h6>In questa sezione è possibile visualizzare gli utenti, modificarli ed eliminarli.</h6></div>
                            </div>
                        </div>
                    </div>
                </div>
            </a>
        </div>

    <div class="col-lg-6 mb-4">
        <a href="index.php?link=12&page=1" style="text-decoration:none;">
            <div class="card bg-success text-white shadow" style="min-height: 120px;">
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-2" align="center" style="margin-bottom: 5px">
                            <i class="fas fa-briefcase fa-5x text-white-300"></i>
                        </div>
                        <div class="col-md-10">
                            <h5>Gestione Aziende</h5>
                            <div class="text-white-50 small"><h6>In questa sezione è possibile visualizzare le aziende, modificarle ed eliminarle.</h6></div>
                        </div>
                    </div>
                </div>
            </div>
        </a>
    </div>

    <div class="col-lg-6 mb-4">
        <a href="index.php?link=2&page=1" style="text-decoration:none;">
            <div class="card bg-info text-white shadow" style="min-height: 120px;">
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-2" align="center" style="margin-bottom: 5px">
                            <i class="fas fa-leaf fa-5x text-white-300"></i>
                        </div>
                        <div class="col-md-10">
                            <h5>Gestione Campi</h5>
                            <div class="text-white-50 small"><h6>In questa sezione è possibile visualizzare i campi, modificarli ed eliminarli.</h6></div>
                        </div>
                    </div>
                </div>
            </div>
        </a>
    </div>

    <div class="col-lg-6 mb-4">
        <a href="index.php?link=6&page=1" style="text-decoration:none;">
            <div class="card bg-warning text-white shadow" style="min-height: 120px;">
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-2" align="center" style="margin-bottom: 5px">
                            <i class="fab fa-accusoft fa-5x text-white-300"></i>
                        </div>
                        <div class="col-md-10">
                            <h5>Gestione Impianti</h5>
                            <div class="text-white-50 small"><h6>In questa sezione è possibile visualizzare gli impianti, modificarli ed eliminarli.</h6></div>
                        </div>
                    </div>
                </div>
            </div>
        </a>
    </div>

</div>




<!-- /.container-fluid -->