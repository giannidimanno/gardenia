<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <meta name="robots" content="noindex">
    <meta name="googlebot" content="noindex">

    <title>Idroponica - Dashboard</title>

    <!-- Favicons -->
    <link rel="shortcut icon" href="img/icon-profile.png" type="image/x-icon">

    <!-- Custom fonts for this template-->
    <link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">

    <!-- Custom styles for this template-->
    <link href="css/sb-admin-2.css" rel="stylesheet">

    <!-- Custom styles for this page -->
    <link href="vendor/datatables/dataTables.bootstrap4.css" rel="stylesheet">

    <!-- CSS CUSTOM -->
    <link rel="stylesheet" href="css/style.css?versione=<?php echo time(); ?>">



    <!-- Loader -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>
    <script type="text/javascript">
        $(window).load(function() {
            $(".loader").fadeOut("slow");
        });
    </script>

    <link href="js/select2Plugin/css/select2.min.css" rel="stylesheet" />
    <script src="js/select2Plugin/js/select2.min.js"></script>
 
    <script type="text/javascript" src='https://maps.googleapis.com/maps/api/js?sensor=false&libraries=places&key=AIzaSyA8tdadDH1CSoGBf_c-4QGgobOgLKkeoBw'></script>


</head>
