<?php

include '../db_configuration/config.php';

$link = mysqli_connect(DatiDb::db_server, DatiDb::db_username, DatiDb::db_password);


if (!$link) {
	die ('Non riesco a connettermi: ' . mysqli_error());
}

$db_selected = mysqli_select_db( $link,DatiDb::db_name);

if (!$db_selected) {
	die ("Errore nella selezione del database: " . mysql_error());
}
$like = mysqli_escape_string($link,$_GET['q']);
$query="select campi.id as id,aziende.ragione_sociale as ragione_sociale,campi.luogo as luogo  
from campi,aziende
where campi.id_azienda=aziende.id
and (aziende.ragione_sociale like '%$like%' or campi.luogo like '%$like%') and campi.flag_eliminato=0";
$result = mysqli_query($link,$query);

$json = [];
while($row = mysqli_fetch_array($result, MYSQLI_ASSOC)){
     $json[] = ['id'=>($row['id']), 'text'=>($row['ragione_sociale'].' - '.$row['luogo'])];
}
echo json_encode($json,JSON_UNESCAPED_UNICODE);

?>

