(function($) {
  "use strict"; // Start of use strict

  // Toggle the side navigation
  $("#sidebarToggle, #sidebarToggleTop").on('click', function(e) {
    $("body").toggleClass("sidebar-toggled");
    $(".sidebar").toggleClass("toggled");
    if ($(".sidebar").hasClass("toggled")) {
      $('.sidebar .collapse').collapse('hide');
    };
  });

  // Close any open menu accordions when window is resized below 768px
  $(window).resize(function() {
    if ($(window).width() < 768) {
      $('.sidebar .collapse').collapse('hide');
    };
  });

  // Prevent the content wrapper from scrolling when the fixed side navigation hovered over
  $('body.fixed-nav .sidebar').on('mousewheel DOMMouseScroll wheel', function(e) {
    if ($(window).width() > 768) {
      var e0 = e.originalEvent,
        delta = e0.wheelDelta || -e0.detail;
      this.scrollTop += (delta < 0 ? 1 : -1) * 30;
      e.preventDefault();
    }
  });

  // Scroll to top button appear
  $(document).on('scroll', function() {
    var scrollDistance = $(this).scrollTop();
    if (scrollDistance > 100) {
      $('.scroll-to-top').fadeIn();
    } else {
      $('.scroll-to-top').fadeOut();
    }
  });

  // Smooth scrolling using jQuery easing
  $(document).on('click', 'a.scroll-to-top', function(e) {
    var $anchor = $(this);
    $('html, body').stop().animate({
      scrollTop: ($($anchor.attr('href')).offset().top)
    }, 1000, 'easeInOutExpo');
    e.preventDefault();
  });



  //setto valore sulla modale
  $('#modaleCampo').on('show.bs.modal', function(e) {
    var id = e.relatedTarget.dataset.id;
    $("#modaleCampo #button-modal-value-delete-campo").val( id );
  });

  //setto valore sulla modale
  $('#modaleimpianto').on('show.bs.modal', function(e) {
    var id = e.relatedTarget.dataset.id;
    $("#modaleimpianto #button-modal-value-delete-impianto").val( id );
  });

  //setto valore sulla modale
  $('#modaleAzienda').on('show.bs.modal', function(e) {
    var id = e.relatedTarget.dataset.id;
    $("#modaleAzienda #button-modal-value-delete-azienda").val( id );
  });
  //setto valore sulla modale
  $('#modaleDispositivo').on('show.bs.modal', function(e) {
      var id = e.relatedTarget.dataset.id;
      $("#modaleDispositivo #button-modal-value-delete-dispositivo").val( id );
  });
  //setto valore sulla modale
  $('#modaleUtente').on('show.bs.modal', function(e) {
    var id = e.relatedTarget.dataset.id;
    $("#modaleUtente #button-modal-value-delete-utente").val( id );
  });



  $("#id_utente").select2({
    placeholder: "Cerca per Nome o Cognome",
    allowClear: true,
    ajax: {
      url: "../ajax/ricercaUtente.php",
      dataType: 'json',
      delay: 250,
      data: function (params) {
        console.log(params.term);
        return {
          q: params.term
        };
      },
      processResults: function (data) {
        return {
          results: data
        };
      },
      cache: true
    },
    minimumInputLength: 3
  });


  $('#id_utente').on('select2:select', function (e) {
    var data = e.params.data;

    $.ajax({
      datatype: "json",
      url: "../ajax/recuperaUtente.php",
      data: {
        id: data.id
      },
      beforeSend: function() {

      },
      success: function(data) {

        var obj = $.parseJSON(data);
        console.log(obj)
      }
    });
  });



  $("#id_azienda").select2({
    placeholder: "Cerca per Ragione Sociale",
    allowClear: true,
    ajax: {
      url: "../ajax/ricercaAzienda.php",
      dataType: 'json',
      delay: 250,
      data: function (params) {
        console.log(params.term);
        return {
          q: params.term
        };
      },
      processResults: function (data) {
        return {
          results: data
        };
      },
      cache: true
    },
    minimumInputLength: 3
  });


  $('#id_azienda').on('select2:select', function (e) {
    var data = e.params.data;

    $.ajax({
      datatype: "json",
      url: "../ajax/recuperaAzienda.php",
      data: {

        id: data.id
      },
      beforeSend: function() {

      },
      success: function(data) {

        var obj = $.parseJSON(data);
        console.log(obj)
      }
    });
  });


  $("#id_campo").select2({
    placeholder: "Cerca per Campo",
    allowClear: true,
    ajax: {
      url: "../ajax/ricercaCampo.php",
      dataType: 'json',
      delay: 250,
      data: function (params) {
        console.log(params.term);
        return {
          q: params.term
        };
      },
      processResults: function (data) {
        return {
          results: data
        };
      },
      cache: true
    },
    minimumInputLength: 3
  });


  $('#id_campo').on('select2:select', function (e) {
    var data = e.params.data;

    $.ajax({
      datatype: "json",
      url: "../ajax/recuperaCampo.php",
      data: {

        id: data.id
      },
      beforeSend: function() {

      },
      success: function(data) {

        var obj = $.parseJSON(data);
        console.log(obj)
      }
    });
  });






})(jQuery); // End of use strict



function deleteCampo() {
  var id=document.getElementById('button-modal-value-delete-campo').value;
  window.location.href = "action_delete_campo.php?id="+id;
}

function deleteImpianti() {
  var id=document.getElementById('button-modal-value-delete-impianto').value;
  window.location.href = "action_delete_impianto.php?id="+id;
}

function deleteAzienda() {
  var id=document.getElementById('button-modal-value-delete-azienda').value;
  window.location.href = "action_delete_azienda.php?id="+id;
  console.log(id);
}

function deleteUtenti() {
  var id=document.getElementById('button-modal-value-delete-utente').value;
  window.location.href = "action_delete_utente.php?id="+id;
  console.log(id);
}



// -------GEOCODING------
function init() {
    var input = document.getElementById('pac-input');
    var autocomplete = new google.maps.places.Autocomplete(input);
    google.maps.event.addListener(autocomplete, 'place_changed', function () {
        var place = autocomplete.getPlace();
        initMap(place.geometry.location.lat(),place.geometry.location.lng());
    });
}

function initMap(lat, lng) {
  const myLatLng = { lat: lat, lng: lng };
  var posizione = new google.maps.LatLng(lat, lng);
  const map = new google.maps.Map(document.getElementById("map"), {
    zoom: 18,
    center: posizione,
  });
  new google.maps.Marker({
    position: myLatLng,
    map,
    title: "Il tuo campo",
  });
  var inputLat = document.getElementById('id-lat');
  var inputLng = document.getElementById('id-lng');
  inputLat.value=lat;
  inputLng.value=lng;
}


// PER MAPPA IN MODIFICA CAMPO--------------



function showMaps() {
  var latitudine = parseFloat(document.getElementById('id-lat').value);
  var longitudine = parseFloat(document.getElementById('id-lng').value);
  console.log('latitudine: '+latitudine);
  console.log('longitudine: '+longitudine);
  initMap(latitudine, longitudine);
  /*
  const myLatLng = { lat: latitudine, lng: longitudine};
  const map = new google.maps.Map(document.getElementById("map"), {
    zoom: 18,
    center: myLatLng,
  });
  new google.maps.Marker({
    position: myLatLng,
    map,
    title: "Hello World!",
  });
  */
}


// -------TOGGLE DISPLAY MAPPA----
function showElement() {
  var x = document.getElementById("map");
  if (x.style.display == "none") {
    x.style.display = "block";
  } else {
    x.style.display = "none";
  }
}






