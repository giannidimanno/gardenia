// -------- GRAFICO-->
    google.charts.load('current', {'packages':['corechart']});

    // LIVELLO ACQUA CHART----
    google.charts.setOnLoadCallback(drawLivChart);
    function drawLivChart() {
        var data = google.visualization.arrayToDataTable([
        ['Ore', '% acqua'],
        ['12',  40],
        ['14',  50],
        ['16',  80],
        ['18',  60]
        ]);

        var options = {
            hAxis: {title: 'Ore',  titleTextStyle: {color: '#17a2b8'}},
            vAxis: {minValue: 0},
            colors: ['#17a2b8'],
            chartArea: {
            // leave room for y-axis labels
            width: '85%'
            },      
            legend: {
                position: 'top'
                },          
            };

        var chart = new google.visualization.AreaChart(document.getElementById('chart_liv'));
        chart.draw(data, options);
    }

    
    // TEMP CHART----
    google.charts.setOnLoadCallback(drawTempChart);
    function drawTempChart() {
        var data = google.visualization.arrayToDataTable([
        ['Ore', '°C'],
        ['12',  18],
        ['14',  19],
        ['16',  20],
        ['18',  21]
        ]);

        var options = {
            hAxis: {title: 'Ore',  titleTextStyle: {color: '#e0440e'}},
            vAxis: {minValue: 0},
            colors: ['#e0440e'],
            chartArea: {
            // leave room for y-axis labels
            width: '80%'
            },      
            legend: {
                position: 'top'
                },          
            };

        var chart = new google.visualization.AreaChart(document.getElementById('chart_temp'));
        chart.draw(data, options);
    }
    
    // PH CHART----
    google.charts.setOnLoadCallback(drawPhChart);
    function drawPhChart() {
        var data = google.visualization.arrayToDataTable([
        ['Ore', 'PH'],
        ['12',  4],
        ['14',  4.5],
        ['16',  5],
        ['18',  5.93]
        ]);

        var options = {
        // title: 'PH',
        hAxis: {title: 'Ore',  titleTextStyle: {color: '#007bff'}},
        vAxis: {minValue: 0},
        colors: ['#007bff'],
        chartArea: {
            // leave room for y-axis labels
            width: '85%'
            },      
            legend: {
                position: 'top'
                },          
            

        };

        var chart = new google.visualization.AreaChart(document.getElementById('chart_ph'));
        chart.draw(data, options);
    }

    // graf CONDUCIBILITÀ----
    google.charts.setOnLoadCallback(drawCondChart);
    function drawCondChart() {
        var data = google.visualization.arrayToDataTable([
        ['Ore', 'uS'],
        ['12',  1000],
        ['14',  1100],
        ['16',  1090],
        ['18',  1200]
        ]);

        var options = {
        // title: 'PH',
        hAxis: {title: 'Ore',  titleTextStyle: {color: '#ffc107'}},
        vAxis: {minValue: 0},
        colors: ['#ffc107'],
        chartArea: {
            // leave room for y-axis labels
            width: '85%'
            },      
            legend: {
                position: 'top'
                },          
            

        };

        var chart = new google.visualization.AreaChart(document.getElementById('chart_cond'));
        chart.draw(data, options);
    }
