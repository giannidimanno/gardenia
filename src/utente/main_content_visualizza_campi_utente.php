<?php

$dbMgr=new DbManager();
$impiantiMgr = new impiantiMgr($dbMgr);
$campiMgr = new CampiMgr($dbMgr);
try{
    $campi=$campiMgr->getCampiByIdUtente($_SESSION['id_utente']);
}catch(Exception $e){
    $campi=[];
}


?>





<div class="row">
    <div class="col-lg-12">
        <!-- Circle Buttons -->
        <div class="card shadow mb-4 border-left-primary">
            <div class="card-body" style="overflow-x: auto">

                <h1 class="h3 mb-2 text-gray-800">Visualizza Campi</h1>
                <p class="mb-4">In questa sezione è possibile visualizzare tutti i tuoi campi.</p>
                <form action="index.php?" method="get">

                </form>

                <table class="table table-bordered" width="100%" cellspacing="0">
                    <thead>
                    <tr>
                        <th>Id</th>
                        <th style="min-width:200px;">Luogo</th>
                        <th>Dimensioni</th>
                        <th>Latitudine</th>
                        <th>Longitudine</th>
                    </tr>
                    </thead>

                    <tbody>



                    <?php

                    foreach($campi as $campo){
                        echo '
                        <tr>
                            <td>'.$campo->getId().'</td>
                            <td>'.$campo->getLuogo().'</td>
                            <td>'.$campo->getDimensione().'</td>
                            <td>'.$campo->getLatitudine().'</td>
                            <td>'.$campo->getLongitudine().'</td>
                            
                    </tr>
                        ';
                    }

                    ?>



                    </tbody>
                </table>
                <br />



            </div>
        </div>
    </div>
</div>
