<html>
    <head>
    <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
    <script type="text/javascript">
      google.charts.load('current', {'packages':['corechart']});
    google.charts.setOnLoadCallback(drawChart);
    function drawChart() {

    //GRAFICO 1
    var data1 = google.visualization.arrayToDataTable([
      ['Ore', 'Temp'],
      ['04',  20],
      ['08',  23],
      ['12',  25],
      ['15',  21],
      ['18',  22],
      ['22',  23]
    ]);

    var options1 = {
      title: 'Temperatura Substrato',
      hAxis: {title: 'Ore',  titleTextStyle: {color: '#333'}},
      vAxis: {minValue: 0}
    };
    
    var chart1 = new google.visualization.AreaChart(document.getElementById('chart_temp'));
    chart1.draw(data1, options1);


    //GRAFICO 2
    var data2 = google.visualization.arrayToDataTable([
        ['Ore', 'Umid'],
      ['04',  40],
      ['08',  44],
      ['12',  48],
      ['15',  56],
      ['18',  60],
      ['22',  47]
      
    ]);

    var options2 = {
      title: 'Umidità Substrato',
      hAxis: {title: 'Ore',  titleTextStyle: {color: '#333'}},
      vAxis: {minValue: 0},
      colors: ['red']
    };
    var chart2 = new google.visualization.AreaChart(document.getElementById('chart_um'));
    chart2.draw(data2, options2);

    
    //GRAFICO 3
    var data3 = google.visualization.arrayToDataTable([
      ['Ore', 'Condut'],
      ['04',  450],
      ['08',  476],
      ['12',  480],
      ['15',  530],
      ['18',  500],
      ['22',  480]
    ]);

    var options3 = {
      title: 'Conducibilità Substrato',
      hAxis: {title: 'Ore',  titleTextStyle: {color: '#333'}},
      vAxis: {minValue: 0},
      colors: ['green']
    };
    var chart3 = new google.visualization.AreaChart(document.getElementById('chart_cond'));
    chart3.draw(data3, options3);
    
  }
    </script>
    </head>


<body>
<div class="container-fluid">
    <div class="row">

        <!-- Earnings (Monthly) Card Example -->
        <div class="col-xs-12 col-md-12 col lg-4 col-xl-4 mb-4">
            <div class="card border-left-primary shadow h-100 py-2">
                <div class="card-body" style = "height: 460px;">
                <div class="row no-gutters align-items-center">
                    <div class="col mr-2">
                        <div class="text-xs font-weight-bold text-primary text-center mb-1" style="font-size:33px;">Temperatura del Substrato <br></div>
                        <div class="h1 mb-0 font-weight-bold text-gray-800 text-center"> 23°C</div><br>
                    </div>
                    <div  id="chart_temp" style="width: 100%; height: 300px;"></div>
                </div>
                </div>
            </div>
        </div>

        <div class="col-xl-4 col-md-12 mb-4">
            <div class="card border-left-danger shadow h-100 py-2">
                <div class="card-body">
                    <div class="row no-gutters align-items-center">
                        <div class="col mr-2">
                        <div class="text-xs font-weight-bold text-danger text-center mb-1" style="font-size:33px;">Umidità del Substrato <br></div>
                        <div class="h1 mb-0 font-weight-bold text-gray-800 text-center"> 47% </div><br>
                        </div>
                        <div id="chart_um" style="width: 100%; height: 300px;"></div>
                    </div>
                </div>
            </div>
        </div>

        <!-- Pending Requests Card Example -->
        <div class="col-xl-4 col-md-12 mb-4">
            <div class="card border-left-success shadow h-100 py-2 hidden">
                <div class="card-body">
                <div class="row no-gutters align-items-center">
                    <div class="col mr-2">
                    <div class="text-xs font-weight-bold text-success text-center mb-1" style="font-size:33px;">Conducibilità del Substrato <br></div>
                    <div class="h1 mb-0 font-weight-bold text-gray-800 text-center"> 480 μS </div><br>
                    </div>
                    <div id="chart_cond" style="width: 100%; height: 300px;"></div>
                </div>
                </div>
            </div>
        </div>
    </div>


    <div class="row">
       <div class="col-8">
        <div class="card border-left-warning shadow h-100 py-2">
            <div class="card-body">
            <h2 class="text-center text-warning"><b>Programma di Irrigazione</b></h2>
            <h6 class="text-center">Qui puoi scegliere i turni di irrigazione giornalieri, inoltre puoi disattivarli in qualsiasi momento</h6><br>
            <table align="center" border="3" cellspacing="0">
                <!--<caption>Timetable</caption>-->
                <tr>
                    <td align="center" height="50"
                        width="125">
                        <b>Ciclo 1</b></br>
                    </td>
                    <td align="center" height="50"
                        width="125">
                        <b>Ciclo 2</b>
                    </td>
                    <td align="center" height="50"
                        width="125">
                        <b>Ciclo 3</b>
                    </td>
                    <td align="center" height="50"
                        width="125">
                        <b>Ciclo 4</b>
                    </td>
                    <td align="center" height="50"
                        width="125">
                        <b>Ciclo 5</b>
                    </td>
                    <td align="center" height="50"
                        width="125">
                        <b>Ciclo 6</b>
                    </td>
                    <td align="center" height="50"
                        width="125">
                        <b>Ciclo 7</b>
                    </td>
                </tr>
                <tr>
                <td align="center" height="50">
                    <select name="Stato1" id="Stato1">
                        <option value="00:00" selected>00:00</option>
                        <option value="01:00">01:00</option>
                        <option value="02:00">02:00</option>
                        <option value="03:00">03:00</option>
                        <option value="04:00">04:00</option>
                        <option value="05:00">05:00</option>
                        <option value="06:00">06:00</option>
                        <option value="07:00">07:00</option>
                        <option value="08:00">08:00</option>
                        <option value="09:00">09:00</option>
                        <option value="10:00">10:00</option>
                        <option value="11:00">11:00</option>
                        <option value="12:00">12:00</option>
                        <option value="13:00">13:00</option>
                        <option value="14:00">14:00</option>
                        <option value="15:00">15:00</option>
                        <option value="16:00">16:00</option>
                        <option value="17:00">17:00</option>
                        <option value="18:00">18:00</option>
                        <option value="19:00">19:00</option>
                        <option value="20:00">20:00</option>
                        <option value="21:00">21:00</option>
                        <option value="22:00">22:00</option>
                        <option value="23:00">23:00</option>
                    </select>
                </td>
                <td align="center" height="50">
                    <select name="Stato2" id="Stato2">
                        <option value="00:00">00:00</option>
                        <option value="01:00">01:00</option>
                        <option value="02:00">02:00</option>
                        <option value="03:00">03:00</option>
                        <option value="04:00" selected>04:00</option>
                        <option value="05:00">05:00</option>
                        <option value="06:00">06:00</option>
                        <option value="07:00">07:00</option>
                        <option value="08:00">08:00</option>
                        <option value="09:00">09:00</option>
                        <option value="10:00">10:00</option>
                        <option value="11:00">11:00</option>
                        <option value="12:00">12:00</option>
                        <option value="13:00">13:00</option>
                        <option value="14:00">14:00</option>
                        <option value="15:00">15:00</option>
                        <option value="16:00">16:00</option>
                        <option value="17:00">17:00</option>
                        <option value="18:00">18:00</option>
                        <option value="19:00">19:00</option>
                        <option value="20:00">20:00</option>
                        <option value="21:00">21:00</option>
                        <option value="22:00">22:00</option>
                        <option value="23:00">23:00</option>
                    </select>
                </td>
                <td align="center" height="50">
                    <select name="Stato3" id="Stato3">
                        <option value="00:00">00:00</option>
                        <option value="01:00">01:00</option>
                        <option value="02:00">02:00</option>
                        <option value="03:00">03:00</option>
                        <option value="04:00">04:00</option>
                        <option value="05:00">05:00</option>
                        <option value="06:00">06:00</option>
                        <option value="07:00">07:00</option>
                        <option value="08:00" selected>08:00</option>
                        <option value="09:00">09:00</option>
                        <option value="10:00">10:00</option>
                        <option value="11:00">11:00</option>
                        <option value="12:00">12:00</option>
                        <option value="13:00">13:00</option>
                        <option value="14:00">14:00</option>
                        <option value="15:00">15:00</option>
                        <option value="16:00">16:00</option>
                        <option value="17:00">17:00</option>
                        <option value="18:00">18:00</option>
                        <option value="19:00">19:00</option>
                        <option value="20:00">20:00</option>
                        <option value="21:00">21:00</option>
                        <option value="22:00">22:00</option>
                        <option value="23:00">23:00</option>
                    </select>
                </td>
                <td align="center" height="50">
                    <select name="Stato4" id="Stato4">
                        <option value="00:00">00:00</option>
                        <option value="01:00">01:00</option>
                        <option value="02:00">02:00</option>
                        <option value="03:00">03:00</option>
                        <option value="04:00">04:00</option>
                        <option value="05:00">05:00</option>
                        <option value="06:00">06:00</option>
                        <option value="07:00">07:00</option>
                        <option value="08:00">08:00</option>
                        <option value="09:00">09:00</option>
                        <option value="10:00">10:00</option>
                        <option value="11:00">11:00</option>
                        <option value="12:00" selected>12:00</option>
                        <option value="13:00">13:00</option>
                        <option value="14:00">14:00</option>
                        <option value="15:00">15:00</option>
                        <option value="16:00">16:00</option>
                        <option value="17:00">17:00</option>
                        <option value="18:00">18:00</option>
                        <option value="19:00">19:00</option>
                        <option value="20:00">20:00</option>
                        <option value="21:00">21:00</option>
                        <option value="22:00">22:00</option>
                        <option value="23:00">23:00</option>
                    </select>
                </td>
                <td align="center" height="50">
                    <select name="Stato5" id="Stato5">
                        <option value="00:00" selected>00:00</option>
                        <option value="01:00">01:00</option>
                        <option value="02:00">02:00</option>
                        <option value="03:00">03:00</option>
                        <option value="04:00">04:00</option>
                        <option value="05:00">05:00</option>
                        <option value="06:00">06:00</option>
                        <option value="07:00">07:00</option>
                        <option value="08:00">08:00</option>
                        <option value="09:00">09:00</option>
                        <option value="10:00">10:00</option>
                        <option value="11:00">11:00</option>
                        <option value="12:00">12:00</option>
                        <option value="13:00">13:00</option>
                        <option value="14:00">14:00</option>
                        <option value="15:00" selected>15:00</option>
                        <option value="16:00">16:00</option>
                        <option value="17:00">17:00</option>
                        <option value="18:00">18:00</option>
                        <option value="19:00">19:00</option>
                        <option value="20:00">20:00</option>
                        <option value="21:00">21:00</option>
                        <option value="22:00">22:00</option>
                        <option value="23:00">23:00</option>
                    </select>
                </td>
                <td align="center" height="50">
                    <select name="Stato6" id="Stato6">
                        <option value="00:00">00:00</option>
                        <option value="01:00">01:00</option>
                        <option value="02:00">02:00</option>
                        <option value="03:00">03:00</option>
                        <option value="04:00">04:00</option>
                        <option value="05:00">05:00</option>
                        <option value="06:00">06:00</option>
                        <option value="07:00">07:00</option>
                        <option value="08:00">08:00</option>
                        <option value="09:00">09:00</option>
                        <option value="10:00">10:00</option>
                        <option value="11:00">11:00</option>
                        <option value="12:00">12:00</option>
                        <option value="13:00">13:00</option>
                        <option value="14:00">14:00</option>
                        <option value="15:00">15:00</option>
                        <option value="16:00">16:00</option>
                        <option value="17:00">17:00</option>
                        <option value="18:00" selected>18:00</option>
                        <option value="19:00">19:00</option>
                        <option value="20:00">20:00</option>
                        <option value="21:00">21:00</option>
                        <option value="22:00">22:00</option>
                        <option value="23:00">23:00</option>
                    </select>
                </td>
                <td align="center" height="50">
                    <select name="Stato7" id="Stato7" >
                        <option value="00:00">00:00</option>
                        <option value="01:00">01:00</option>
                        <option value="02:00">02:00</option>
                        <option value="03:00">03:00</option>
                        <option value="04:00">04:00</option>
                        <option value="05:00">05:00</option>
                        <option value="06:00">06:00</option>
                        <option value="07:00">07:00</option>
                        <option value="08:00">08:00</option>
                        <option value="09:00">09:00</option>
                        <option value="10:00">10:00</option>
                        <option value="11:00">11:00</option>
                        <option value="12:00">12:00</option>
                        <option value="13:00">13:00</option>
                        <option value="14:00">14:00</option>
                        <option value="15:00">15:00</option>
                        <option value="16:00">16:00</option>
                        <option value="17:00">17:00</option>
                        <option value="18:00">18:00</option>
                        <option value="19:00">19:00</option>
                        <option value="20:00">20:00</option>
                        <option value="21:00">21:00</option>
                        <option value="22:00" selected>22:00</option>
                        <option value="23:00">23:00</option>
                    </select>
                </td>
                    <!--<td align="center" height="50"><input type="time" style="text-align: center;" value="12:00"></td>-->
                </tr>
                <tr>
                    <td align="center" height="50"><p></p>
                        <label class="switch">
                            <input type="checkbox" checked>
                            <span class="slider round"></span>
                        </label>
                    </td>
                    <td align="center" height="50"><p></p>
                        <label class="switch">
                            <input type="checkbox" checked>
                            <span class="slider round"></span>
                        </label>
                    </td>
                    <td align="center" height="50"><p></p>
                        <label class="switch">
                            <input type="checkbox" checked>
                            <span class="slider round"></span>
                        </label>
                    </td>
                    <td align="center" height="50"><p></p>
                        <label class="switch">
                            <input type="checkbox" checked>
                            <span class="slider round"></span>
                        </label>
                    </td>
                    <td align="center" height="50"><p></p>
                        <label class="switch">
                            <input type="checkbox" checked>
                            <span class="slider round"></span>
                        </label>
                    </td>
                    <td align="center" height="50"><p></p>
                        <label class="switch">
                            <input type="checkbox" checked>
                            <span class="slider round"></span>
                        </label>
                    </td>
                    <td align="center" height="50"><p></p>
                        <label class="switch">
                            <input type="checkbox" checked>
                            <span class="slider round"></span>
                        </label>
                    </td>
                </tr>
            </table>
            </div>
        </div>
    </div>

       <style>
            input[type="number"] {
                 display: block;
                 margin : 0 auto;
            }
        </style>

        <div class="col-4">
            <div class="card border-left-dark shadow h-100 py-2">
                <div  class="card-body text-align:center">
                <h3 class="text-center"><b>Durata Ciclo di Irrigazione</b></h3><p></p>
                <h6 class="text-center">Scegli per quanti minuti le piante dovranno <br> essere irrigate<br> Minimo 1 min, Massimo 30 min</h6><br><br>
                <input type="number" class="text-gray-800" id="textboxid" style="text-align: center;" id="tentacles" name="tentacles"
                min="1" max="30" value="20">
                </div>
            </div>
        </div>


    </div>
</div>
</body>
<style>
table td {
  position: relative;
}

table td input {
  position: absolute;
  display: block;
  top:0;
  left:0;
  margin: 0;
  height: 100%;
  width: 100%;
  border: none;
  padding: 10px;
  box-sizing: border-box;
}

select {
    border: 0;
    width: 101%;
    text-align-last:center;
}
input[type="number"] {
   width:100px;
}

#textboxid
{
    height:50px;
    font-size:20pt;
}


.switch {
  position: relative;
  display: inline-block;
  width: 60px;
  height: 30px;
}

/* Hide default HTML checkbox */
.switch input {
  opacity: 0;
  width: 0;
  height: 0;
}

/* The slider */
.slider {
  position: absolute;
  cursor: pointer;
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
  background-color: #ccc;
  -webkit-transition: .4s;
  transition: .4s;
}

.slider:before {
  position: absolute;
  content: "";
  height: 22px;
  width: 22px;
  left: 6px;
  bottom: 4px;
  background-color: white;
  -webkit-transition: .4s;
  transition: .4s;
}

input:checked + .slider {
  background-color: #2196F3;
}

input:focus + .slider {
  box-shadow: 0 0 1px #2196F3;
}

input:checked + .slider:before {
  -webkit-transform: translateX(26px);
  -ms-transform: translateX(26px);
  transform: translateX(26px);
}

/* Rounded sliders */
.slider.round {
  border-radius: 34px;
}

.slider.round:before {
  border-radius: 50%;
}
</style>
<!--
<td align="center" height="50">
    <select name="Stato3" id="Stato3">
        <option value="Attiva">Attiva</option>
        <option value="Disattiva">Disattivata</option>
    </select>
</td>


var chart3 = new CanvasJS.Chart("chartContainer3",
    {
       data: [
      {
        type: "line",
        color:"green",
        dataPoints: [
        { label: "16:00", y: 510 },
        { label: "20:15", y: 480 },
        { label: "00:00", y: 414 },
        { label: "03:00", y: 520 },
        { label: "06:00", y: 500 },
        { label: "08:00", y: 450 },
        { label: "12:00", y: 480 }
        ]
      }
      ]
    });
    chart.render();
    chart2.render();
    chart3.render();
-->