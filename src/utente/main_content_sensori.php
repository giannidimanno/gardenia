<?php
$id_impianto=$_GET['id_impianto'];
$tab=$_GET['tab'];

?>
<div class="container-fluid">
    <form>
        <ul class="nav nav-tabs">
            <li class="nav-item" role="presentation">
                <?php
                if($tab==1){
                    echo '<a class="nav-link active" id="home-tab"  href="index.php?link=5&id_impianto='.$id_impianto.'&tab=1" role="tab" aria-controls="home" aria-selected="true">Nutrimenti</a>';
                }else{
                    echo '<a class="nav-link" id="home-tab"  href="index.php?link=5&id_impianto='.$id_impianto.'&tab=1" role="tab" aria-controls="home" aria-selected="false">Nutrimenti</a>';
                }
                ?>
            </li>
            <li class="nav-item" role="presentation">
                <?php
                if($tab==2){
                    echo '<a class="nav-link active" id="home-tab" href="index.php?link=5&id_impianto='.$id_impianto.'&tab=2" role="tab" aria-controls="home" aria-selected="true">Substrato</a>';
                }else{
                    echo '<a class="nav-link" id="home-tab"  href="index.php?link=5&id_impianto='.$id_impianto.'&tab=2" role="tab" aria-controls="home" aria-selected="false">Substrato</a>';
                }
                ?>
            </li>
            <li class="nav-item" role="presentation">
                <?php
                if($tab==3){
                    echo '<a class="nav-link active" id="home-tab"  href="index.php?link=5&id_impianto='.$id_impianto.'&tab=3" role="tab" aria-controls="home" aria-selected="true">Ambiente</a>';
                }else{
                    echo '<a class="nav-link" id="home-tab"  href="index.php?link=5&id_impianto='.$id_impianto.'&tab=3" role="tab" aria-controls="home" aria-selected="false">Ambiente</a>';
                }
                ?>
            </li>
        </ul>
    </form>

</div>
<br>


<?php
if($tab==1){
    include 'main_content_dashboard_nutrienti.php';
}elseif($tab==2){
    include 'main_content_dashboard_substrato.php';
}elseif($tab==3){
    include 'main_content_dashboard_ambiente.php';
}

?>
