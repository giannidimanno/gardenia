<html>
    <head>
        <!-- STYLE SLIDER -->
        <link rel="stylesheet" href="../css/slider.css">
        <!-- JS GRAFICI -->
        <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
        <script src="../js/charts.js"></script>
    </head>
    <body>
        <div class="container-fluid">
                <!-- -----------NAVBAR----------- -->
                <!-- <div class="row mb-5">
                    <div class="col-12 mt-3">
                        <ul class="nav border">
                            <li class="nav-item">
                            <a class="nav-link fs-6 active" aria-current="page" href="#">Nutrienti</a>
                            </li>
                            <li class="nav-item">
                            <a class="nav-link fs-6 disabled" href="#">Substrato</a>
                            </li>
                            <li class="nav-item">
                            <a class="nav-link fs-6 disabled" href="#">Ambiente</a>
                            </li>
                            <li class="nav-item">
                            <a class="nav-link fs-6 disabled" href="#" tabindex="-1" aria-disabled="true">Diagnostica</a>
                            </li>
                        </ul>
                    </div>
                </div> -->

                <!-- -----------TEMPERATURA ACQUA-------------- -->
                <div class="row">
                    <div class="col-lg-12 col-md-12 col-sm-12">
                        <div class="card shadow mb-4 border-left-danger h-200 py-2">
                            <div class="card-body">
                                <div class="row no-gutters align-items-center ">
                                    <div class="col mr-2">
                                        <div class="text-xs font-weight-bold text-danger text-center" style="font-size:33px;">
                                            TEMPERATURA ACQUA
                                        </div>
                                        <h1 class="card-title text-center font-weight-bold text-dark">20 °C</h1>

                                        <div id="chart_temp" style="width: 100%; width: 100%;"></div>

                                    </div>
                                </div>
                                <div class="col-8 offset-2">
                                    <!-- SLIDER -->
                                    <!-- <div class="min-max-slider" data-legendnum="2">
                                        <label for="min">Minimum</label>
                                        <input id="min" class="min" name="min" type="range" step="1" min="0" max="100" />
                                        <label for="max">Maximum</label>
                                        <input id="max" class="max" name="max" type="range" step="1" min="0" max="100" />
                                    </div>  -->

                                    <!-- <div class="progress">
                                        <div class="progress-bar progress-bar-striped bg-info" role="progressbar" style="width: 50%" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100">50%</div>
                                    </div> -->
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            
        

                <!-- -----------LIVELLO ACQUA----------- -->
            <div class="row">
                <div class="col-xl-4 col-lg-12 col-md-12 col-sm-12 mt-4">
                    <div class="card mb-4 border-left-info shadow h-100 py-2">
                        <div class="card-body">
                            <div class="row no-gutters align-items-center">
                                <div class="col mr-2">
                                    <div class="text-xs font-weight-bold text-info text-center mb-1" style="font-size:33px;">
                                        LIVELLO ACQUA
                                    </div>
                                </div>
                            </div>
                            <h1 class="card-title text-center font-weight-bold text-gray-800">60 %</h1>
                            <div id="chart_liv"></div>
                        </div>
                        <div class="row d-flex justify-content-center">
                            <div class="col-10">
                                <div class="col-12 mr-2 mb-5">
                                    <p>Regola la percentuale minima e massima di acqua contenuta nella tanica:</p>
                                    <div class="col-12">
                                        <div class="range-slider">
                                            <label for="" class="font-weight-bold">MIN: </label>
                                            <input class="range-slider__range" type="range" value="20" min="0" max="100">
                                            <span class="range-slider__value">0</span>
                                            <span class="font-weight-bold">%</span>
                                        </div>
                                    </div>
                                    <div class="col-12">
                                        <div class="range-slider">
                                            <label for="" class="font-weight-bold">MAX: </label>
                                            <input class="range-slider__range" type="range" value="80" min="0" max="100">
                                            <span class="range-slider__value">0</span>
                                            <span class="font-weight-bold">%</span>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="btn-toolbar  mb-5 mt-5" role="toolbar" aria-label="Toolbar with button groups">
                            <div class="col-xl-12 offset-3 mb-2">
                                <button type="button" class="btn btn-danger">Action 1</button>
                                <button type="button" class="btn btn-danger">Action 2</button>
                            </div>
                            <div class="col-xl-12 offset-3">
                                <button type="button" class="btn btn-danger">Action 3</button>
                                <button type="button" class="btn btn-danger">Action 4</button>
                            </div>
                        </div>
                    </div>
                </div>

                <!-- -----------PH ACQUA----------- -->
                <div class="col-xl-4 col-lg-12 col-md-12 col-sm-12 mt-4">
                    <div class="card border-left-primary shadow h-100 py-2">
                        <div class="card-body">
                            <div class="row no-gutters align-items-center">
                                    <div class="col mr-2">
                                        <div class="text-xs font-weight-bold text-primary text-center mb-1" style="font-size:33px;">
                                            PH ACQUA
                                        </div>
                                    </div>
                            </div>

                                <h1 class="card-title text-center font-weight-bold text-dark">5,93 ph</h1>
                                <div id="chart_ph" style="width: 100%; height: 250px; width: 100%;"></div>
                            </div>
                        <div class="row d-flex justify-content-center">
                            <div class="col-10">
                                <p>Imposta il livello minimo e il livello massimo del PH desiderato:</p>
                                <div class="col-12 mr-2 mb-5">
                                            <div class="col-12">
                                                <div class="range-slider">
                                                    <label for="" class="font-weight-bold">MIN: </label>
                                                    <input class="range-slider__range" type="range" value="5.5" min="0" max="12">
                                                    <span class="range-slider__value">0</span>
                                                    <span class="font-weight-bold">PH</span>

                                                </div>
                                            </div>
                                            <div class="col-12">
                                                <div class="range-slider">
                                                    <label for="" class="font-weight-bold">MAX: </label>
                                                    <input class="range-slider__range" type="range" value="6" min="0" max="12">
                                                    <span class="range-slider__value">0</span>
                                                    <span class="font-weight-bold">PH</span>

                                                </div>
                                            </div>
                                </div>
                            </div>
                        </div>

                        <div class="btn-toolbar  mb-5 mt-5" role="toolbar" aria-label="Toolbar with button groups">
                            <div class="col-xl-12 offset-3 mb-2">
                                <button type="button" class="btn btn-primary">Action 1</button>
                                <button type="button" class="btn btn-primary">Action 2</button>
                            </div>
                            <div class="col-xl-12 offset-3">
                                <button type="button" class="btn btn-primary">Action 3</button>
                                <button type="button" class="btn btn-primary">Action 4</button>
                            </div>
                        </div>

                    </div>
                </div>

                <!-- -----------CONDUCIBILITÀ ACQUA----------- -->
                <div class="col-xl-4 col-lg-12 col-md-12 col-sm-12 mt-4">
                    <div class="card border-left-warning shadow h-100 py-2">
                        <div class="card-body">
                        <div class="row no-gutters align-items-center">
                                    <div class="col mr-2">
                                        <div class="text-xs font-weight-bold text-warning text-center mb-1" style="font-size:33px;">
                                            CONDUCIBILITÀ ACQUA
                                        </div>
                                    </div>
                            </div>

                            <h1 class="card-title text-center font-weight-bold text-dark">1.198 uS</h1>
                            <div id="chart_cond"></div>
                        </div>
                        <div class="row d-flex justify-content-center mb-5">
                            <div class="col-10">
                                <p>Imposta il livello minimo e il livello massimo desiderato della conducibilità:</p>
                                <div class="col-12 mr-2">
                                            <div class="col-12">
                                                <div class="range-slider">
                                                    <label for="" class="font-weight-bold">MIN: </label>
                                                    <input class="range-slider__range" type="range" value="1100" min="0" max="1500">
                                                    <span class="range-slider__value">0</span>
                                                    <span class="font-weight-bold">uS</span>

                                                </div>
                                            </div>
                                            <div class="col-12">
                                                <div class="range-slider">
                                                    <label for="" class="font-weight-bold">MAX: </label>
                                                    <input class="range-slider__range" type="range" value="1200" min="0" max="1500">
                                                    <span class="range-slider__value">0</span>
                                                    <span class="font-weight-bold">uS</span>

                                                </div>
                                            </div>
                                    </div>
                            </div>
                        </div>
                        <div class="btn-toolbar mb-5 mt-5" role="toolbar" aria-label="Toolbar with button groups">
                            <div class="col-xl-12 offset-3 mb-2">
                                <button type="button" class="btn btn-warning">Action 1</button>
                                <button type="button" class="btn btn-warning">Action 2</button>
                            </div>
                            <div class="col-xl-12 offset-3">
                                <button type="button" class="btn btn-warning">Action 3</button>
                                <button type="button" class="btn btn-warning">Action 4</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </body>
    <!-- JS PER GLI SLIDER -->
    <script src="../js/slider.js"></script>
</html>