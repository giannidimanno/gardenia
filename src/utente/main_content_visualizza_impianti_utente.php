<?php
$dbMgr=new DbManager();

$impiantiMgr = new ImpiantiMgr($dbMgr);
$campiMgr = new CampiMgr($dbMgr);

try{
    $impianti = $impiantiMgr->getImpiantiByIdUtente($_SESSION['id_utente']);
}catch(Exception $e){
    $impianti=[];
}


?>

    <div class="row">
    <div class="col-lg-12">
        <!-- Circle Buttons -->
        <div class="card shadow mb-4 border-left-primary">
            <div class="card-body" style="overflow-x: auto">

                <h1 class="h3 mb-2 text-gray-800">Visualizza impianti</h1>
                <p class="mb-4">In questa sezione è possibile visualizzare tutti gli impianti.</p>
                <form action="index.php?" method="get">

                </form>

                <table class="table table-bordered" width="100%" cellspacing="0">
                    <thead>
                    <tr>
                        <th>Id</th>
                        <th style="min-width:200px;">Nome</th>
                        <th>Luogo Campo</th>
                        <th>Dimensioni in MQ</th>
                        <th>Stato</th>
                        <th style="text-align: center;">Visualizza</th>

                    </tr>
                    </thead>

                    <tbody>



                    <?php

                    foreach($impianti as $impianto){
                        $campi = $campiMgr->getCampoById($impianto->getIdCampo());
                        $campo=$campi[0];
                        $luogo = $campo->getLuogo();
                        $stato="Disattiva";
                        if($impianto->getStato()==1){
                            $stato="Attivo";
                        }

                        echo '
                        <tr>
                            <td>'.$impianto->getId().'</td>
                            <td>'.$impianto->getNome().'</td>
                            <td>'.$luogo.'</td>
                            <td>'.$impianto->getDimensioni().'</td>
                            <td>'.$stato.'</td>   
                            <td align="center"><a  href="index.php?link=5&id_impianto='.$impianto->getId().'&tab=1" >
                                    <i class="fas fa-chart-area"></i>
                                </a></td>         
                        </tr>
                        ';  

                        
                    }
                    ?>

                    </tbody>
                </table>
                <br />
            </div>
        </div>
    </div>
</div>
