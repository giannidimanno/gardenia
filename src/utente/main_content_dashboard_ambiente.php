<!DOCTYPE html>
<html lang="en">
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <style>
        .switch {
            position: relative;
            display: inline-block;
            width: 60px;
            height: 34px;
        }

        .switch input {
            opacity: 0;
            width: 0;
            height: 0;
        }

        .slider {
            position: absolute;
            cursor: pointer;
            top: 0;
            left: 0;
            right: 0;
            bottom: 0;
            background-color: #ccc;
            -webkit-transition: .4s;
            transition: .4s;
        }

        .slider:before {
            position: absolute;
            content: "";
            height: 26px;
            width: 26px;
            left: 4px;
            bottom: 4px;
            background-color: white;
            -webkit-transition: .4s;
            transition: .4s;
        }

        input:checked + .slider {
            background-color: #2196F3;
        }

        input:focus + .slider {
            box-shadow: 0 0 1px #2196F3;
        }

        input:checked + .slider:before {
            -webkit-transform: translateX(26px);
            -ms-transform: translateX(26px);
            transform: translateX(26px);
        }

        /* Rounded sliders */
        .slider.round {
            border-radius: 34px;
        }

        .slider.round:before {
            border-radius: 50%;
        }
    </style>

    <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
    <script type="text/javascript">
    google.charts.load('current', {'packages':['corechart']});
    google.charts.setOnLoadCallback(drawChart);
    function drawChart(){

        //grafico 1
        var data1 = google.visualization.arrayToDataTable([
        ['Ore', 'Temp'],
            ['00',  10],
            ['12',  40],
            ['18',  20],
            ['20',  18],
        ]);

        var options1 = {
        title: 'Temp',
        hAxis: {title: 'Ore',  titleTextStyle: {color: '#333'}},
        vAxis: {minValue: 0}
        };
        var chart1 = new google.visualization.AreaChart(document.getElementById('chart_ph'));
        chart1.draw(data1, options1);

        //grafico 2

        var data2 = google.visualization.arrayToDataTable([
            ['Ore', 'Umd'],
            ['00',  20],
            ['12',  60],
            ['18',  40],
            ['20',  70],
        ]);

        var options2 = {
            title: 'Umd',
            hAxis: {title: 'Ore',  titleTextStyle: {color: '#333'}},
            vAxis: {minValue: 0}
        };
        var chart2 = new google.visualization.AreaChart(document.getElementById('chart_ph2'));
        chart2.draw(data2, options2);

        //grafico 3
        var data3 = google.visualization.arrayToDataTable([
            ['Ore', 'Lux'],
            ['00',  5000],
            ['12',  2000],
            ['18',  4000],
            ['20',  8000],
        ]);

        var options3 = {
            title: 'Lux',
            hAxis: {title: 'Ore',  titleTextStyle: {color: '#333'}},
            vAxis: {minValue: 0}
        };

        var chart3 = new google.visualization.AreaChart(document.getElementById('chart_ph3'));
        chart3.draw(data3, options3);

    }
    </script>
</head>
<body>
<div class="container-fluid">
    <div class="row">
            <div class="col-lg-4">
                <div class="col-xl-12 col-md-12 mb-12">
                    <div class="card border-left-warning shadow h-20 py-15">
                        <div class="card-body">
                            <div class="row no-gutters align-items">
                                <div class="col mr-5">
                                    <div class="text font-weight-bold text-warning text-uppercase mb-1">Programma Video </div>
                                    <center>
                                        <text> in questa area è possibile visualizzare lo stato delle piante in tempo reale </text><br>
                                    </center>
                                    <br>
                                    <div align="center">
                                        <button type=”submit”> cam 1 </button>
                                        <button type=”submit”> cam 2 </button>
                                        <button type=”submit”> cam 3 </button>
                                        <button type=”submit”> cam 4 </button>
                                    </div>
                                    <div class="col-auto">
                                    </div>
                                </div>
                            </div>
                            <br>
                            <img src="../img/preview_ambiente.jpg" width="100%">
                        </div>
                    </div>

                </div>
            </div>
            <div class="col-lg-8">
                <div class="row">
                    <!-- Earnings (Monthly) Card Example -->
                    <div class="col-xl-4 col-md-4 mb-4">
                        <div class="card border-left-danger shadow h-20 py-15">
                            <div class="card-body">
                                <div class="row no-gutters align-items">
                                    <div class="col mr-4">
                                        <div class="text font-weight-bold text-danger text-uppercase mb-1">Temperatura Ambientale </div>
                                        <center>
                                            <text class="text font-weight-bold text-danger text-uppercase mb-1">20 °C</text><br>
                                        </center>
                                            <div id="chart_ph" style="width: 100%; height: 250px;"></div>
                                            <div class="col-auto">
                                            </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-4 col-md-4 mb-4">
                        <div class="card border-left-success shadow h-20 py-15">
                            <div class="card-body">
                                <div class="row no-gutters align-items">
                                    <div class="col mr-5">
                                        <div class="text font-weight-bold text-success text-uppercase mb-1">Umidità Ambientale </div>
                                        <center>
                                            <text class="text font-weight-bold text-success text-uppercase mb-1">41%</text><br>
                                        </center>
                                            <div id="chart_ph2" style="width: 100%; height: 250px;"></div>

                                        <div class="col-auto">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Pending Requests Card Example -->
                    <div class="col-xl-4 col-md-4 mb-4">
                        <div class="card border-left-info shadow h-20 py-15">
                            <div class="card-body">
                                <div class="row no-gutters align-items">
                                    <div class="col mr-5">
                                        <div class="text font-weight-bold text-info text-uppercase mb-1">Luminosità Ambientale </div>
                                        <center>
                                            <text class="text font-weight-bold text-info text-uppercase mb-1">5000 LUX</text><br>
                                        </center>
                                            <div id="chart_ph3" style="width: 100%; height: 250px;"></div>
                                        <div class="col-auto">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xl-12 col-md-12 mb-12">
                        <div class="card border-left-primary shadow h-20 py-15">
                            <div class="card-body">
                                <div class="row no-gutters align-items">
                                    <div class="col mr-5">
                                        <div class="text font-weight-bold text-primary text-uppercase mb-1">Programma Illuminazione </div>
                                        <center>
                                            <text> in questa area è possibile impostare il programma di illuminazione </text><br>

                                        <table align="center" border="1" cellspacing="0">
                                            <!--<caption>Timetable</caption>-->
                                            <tr>
                                                <td align="center" height="40"
                                                    width="250">
                                                    <b>Ciclo 1</b></br>
                                                </td>
                                                <td align="center" height="40"
                                                    width="250">
                                                    <b>Ciclo 2</b>
                                                </td>
                                                <td align="center" height="40"
                                                    width="250">
                                                    <b>Ciclo 3</b>
                                                </td>
                                                <td align="center" height="40"
                                                    width="250">
                                                    <b>Ciclo 4</b>
                                                </td>
                                                <td align="center" height="40"
                                                    width="250">
                                                    <b>Ciclo 5</b>
                                                </td>
                                                <td align="center" height="40"
                                                    width="250">
                                                    <b>Ciclo 6</b>
                                                </td>
                                                <td align="center" height="40"
                                                    width="250">
                                                    <b>Ciclo 7</b>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="center" height="50"><input type="time" style="text-align: center;" value="16:00"></td>
                                                <td align="center" height="50"><input type="time" style="text-align: center;" value="20:00"></td>
                                                <td align="center" height="50"><input type="time" style="text-align: center;" value="00:00"></td>
                                                <td align="center" height="50"><input type="time" style="text-align: center;" value="03:00"></td>
                                                <td align="center" height="50"><input type="time" style="text-align: center;" value="06:00"></td>
                                                <td align="center" height="50"><input type="time" style="text-align: center;" value="08:00"></td>
                                                <td align="center" height="50"><input type="time" style="text-align: center;" value="12:00"></td>
                                            </tr>

                                            <tr>
                                                <td align="center" height="50"><p></p>
                                                    <label class="switch">
                                                        <input type="checkbox" checked>
                                                        <span class="slider round"></span>
                                                    </label>
                                                </td>
                                                <td align="center" height="50"><p></p>
                                                    <label class="switch">
                                                        <input type="checkbox" checked>
                                                        <span class="slider round"></span>
                                                    </label>
                                                </td>
                                                <td align="center" height="50"><p></p>
                                                    <label class="switch">
                                                        <input type="checkbox" checked>
                                                        <span class="slider round"></span>
                                                    </label>
                                                </td>
                                                <td align="center" height="50"><p></p>
                                                    <label class="switch">
                                                        <input type="checkbox" checked>
                                                        <span class="slider round"></span>
                                                    </label>
                                                </td>
                                                <td align="center" height="50"><p></p>
                                                    <label class="switch">
                                                        <input type="checkbox" checked>
                                                        <span class="slider round"></span>
                                                    </label>
                                                </td>
                                                <td align="center" height="50"><p></p>
                                                    <label class="switch">
                                                        <input type="checkbox" checked>
                                                        <span class="slider round"></span>
                                                    </label>
                                                </td>
                                                <td align="center" height="50"><p></p>
                                                    <label class="switch">
                                                        <input type="checkbox" checked>
                                                        <span class="slider round"></span>
                                                    </label>
                                                </td>
                                            </tr>
                                        </table>
                                        </center>
                                        <div class="col-auto">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<hr>
</body>
</html>
<style>
    table td {
        position: relative;
    }

    table td input {
        position: absolute;
        display: block;
        top:0;
        left:0;
        margin: 0;
        height: 100%;
        width: 100%;
        border: none;
        padding: 10px;
        box-sizing: border-box;
    }
        

</style>