<?php
ob_start();
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
ini_set('memory_limit', '1024M'); // or you could use 1G
error_reporting(E_ALL);
session_start();

include '../db_configuration/db_manager.php';
include "../bean/Campi.php";
include "../bean/Utenti.php";
include "../bean/Impianti.php";
include "../bean/Aziende.php";

include '../manager/CampiMgr.php';
include '../manager/UtentiMgr.php';
include '../manager/ImpiantiMgr.php';
include "../manager/SendMailMgr.php";
include "../manager/AziendeMgr.php";


require_once "../external_library/PHPMailer/PHPMailer/PHPMailer.php";
require_once "../external_library/PHPMailer/PHPMailer/SMTP.php";
require_once "../external_library/PHPMailer/PHPMailer/Exception.php";
?>