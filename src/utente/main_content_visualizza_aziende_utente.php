<?php

$dbMgr = new DbManager();
$aziendeMgr = new AziendeMgr($dbMgr);
$utentiMgr = new UtentiMgr($dbMgr);


try{
    $aziende = $aziendeMgr->getAziendaByIdUtente($_SESSION['id_utente']);
} catch(Exception $e){
    $aziende = [];
}

?>

<div class="row">
    <div class="col-lg-12">
        <!-- Circle Buttons -->
        <div class="card shadow mb-4 border-left-primary">
            <div class="card-body" style="overflow-x: auto">

                <h1 class="h3 mb-2 text-gray-800">Visualizza Aziende</h1>
                <p class="mb-4">In questa sezione è possibile visualizzare tutte le aziende.</p>
                <form action="index.php?" method="get">

                </form>

                <table class="table table-bordered" width="100%" cellspacing="0">
                    <thead>
                    <tr>
                        <th>Id</th>
                        <th style="min-width:200px;">Ragione Sociale</th>
                        <th>Indirizzo</th>
                        <th>Telefono</th>
                        <th>Codice Fiscale</th>
                        <th>Partita Iva</th>
                    </tr>
                    </thead>

                    <tbody>

                    <?php
                
                        foreach($aziende as $azienda){

                            echo '
                            <tr>
                                <td>'.$azienda->getId().'</td>
                                <td>'.$azienda->getRagioneSociale().'</td>
                                <td>'.$azienda->getIndirizzo().'</td>
                                <td>'.$azienda->getTelefono().'</td>
                                <td>'.$azienda->getCodiceFiscale().'</td>
                                <td>'.$azienda->getPartitaIva().'</td>
                                </tr>
                            ';
                        }
                    ?>



                    </tbody>
                </table>
                <br />



            </div>
        </div>
    </div>
</div>
