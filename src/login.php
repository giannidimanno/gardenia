<?php
include 'inclusioni.php';

$errore=false;

if(isset($_REQUEST['submitLogin'])) {
    $dbMgr = new DbManager();
    $dbMgr->startTransaction();
    $utentiMgr = new UtentiMgr($dbMgr);
    $email = $_POST['email'];
    $password = $_POST['password'];

    try {
        $utenti = $utentiMgr->getLogin($email, $password);
        $utente = $utenti[0];

        $_SESSION['logged'] = 'logged';
        $_SESSION['nome'] = $utente->getNome();
        $_SESSION['email'] = $utente->getEmail();
        $_SESSION['id_utente'] = $utente->getId();

        if($utente->getAmministratore()=='si'){
            header('Location: ../amministratore/index.php?link=0');
            exit;
        }else {
            header('Location: ../utente/index.php?link=0');
            exit;
        }

    } catch (Exception $e) {
        $errore = true;


    }
}
?>


<?php include 'head.php'; ?>

<body class="my_background my_bg_color" id="login_bg">

    <div class="container align-self-center">

        <!-- Outer Row -->
        <div class="row justify-content-center ">

            <div class="col-xl-10 col-lg-12 col-md-9 ">

                <div class="card o-hidden border-0 shadow-lg my-5 my_card" >

                    <div class="card-body p-0">
                        <!-- Nested Row within Card Body -->
                        <div class="row ">
                            <!-- <div class="col-lg-6 d-none d-lg-block"></div> -->
                            <div class="col-lg-12">
                                <div class="p-5">
                                    <div class="text-center">
                                        <h1 class="h4  mb-4 my_txt_white">Benvenuto</h1>
                                    </div>
                                    <form class="user" method="POST" action="login.php">
                                        
                                        <div class="form-group my_form-group">
                                            <input type="email" class="form-control form-control-user"
                                                id="email" aria-describedby="emailHelp"
                                                placeholder="Enter Email Address..." name="email">
                                        </div>
                                        <div class="form-group my_form-group">
                                            <input type="password" class="form-control form-control-user"
                                                id="password" placeholder="Password" name="password">
                                        </div>

                                        <button type="submit" name="submitLogin" class="my_btn btn my_bg_color  btn-user btn-block">Accedi</button>

                                    </form>

                                    <?php
                                        if($errore==true){
                                            echo "<h3>Dati errati</h3>";
                                        }
                                    ?>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>

        </div>

    </div>

    <!-- Bootstrap core JavaScript-->
    <script src="vendor/jquery/jquery.min.js"></script>
    <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

    <!-- Core plugin JavaScript-->
    <script src="vendor/jquery-easing/jquery.easing.min.js"></script>

    <!-- Custom scripts for all pages-->
    <script src="js/sb-admin-2.min.js"></script>

</body>

</html>