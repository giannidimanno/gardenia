SET FOREIGN_KEY_CHECKS = 0;

SET FOREIGN_KEY_CHECKS = 1;



CREATE TABLE utenti (
    id INT NOT NULL AUTO_INCREMENT,
    nome VARCHAR(45) NOT NULL,
    cognome VARCHAR(45) NOT NULL,
    password VARCHAR(255),
    email VARCHAR(50) NOT NULL,
    telefono VARCHAR(50),
    amministratore VARCHAR(2) NOT NULL,
    flag_eliminato int DEFAULT 0,
    data_eliminazione datetime DEFAULT NULL,
    PRIMARY KEY (id)
);

CREATE TABLE aziende (
    id INT NOT NULL AUTO_INCREMENT,
    ragione_sociale VARCHAR(255) NOT NULL,
    indirizzo VARCHAR(100) NOT NULL,
    telefono VARCHAR(50) NOT NULL,
    codice_fiscale VARCHAR(200) DEFAULT NULL,
    partita_iva VARCHAR(200) DEFAULt NULL,
    id_utente INT,
    PRIMARY KEY (id),
    FOREIGN KEY (id_utente) REFERENCES utenti(id),
    flag_eliminato int DEFAULT 0,
    data_eliminazione datetime DEFAULT NULL
);

CREATE TABLE campi(
    id INT NOT NULL AUTO_INCREMENT,
    luogo VARCHAR(255) NOT NULL,
    latitudine FLOAT(8,6) DEFAULT 0,
    longitudine FLOAT(9,6) DEFAULT 0,
    dimensione VARCHAR(255) NOT NULL,
    id_azienda INT NOT NULL,
    PRIMARY KEY (id),
    FOREIGN KEY (id_azienda) REFERENCES aziende (id),
    flag_eliminato int DEFAULT 0,
    data_eliminazione datetime DEFAULT NULL
);

CREATE TABLE impianti(
    id INT(6) NOT NULL AUTO_INCREMENT ,
    nome VARCHAR(30) NOT NULL,
    dimensioni VARCHAR(20) NOT NULL,
    stato VARCHAR(2) NOT NULL,
    id_campo INT(6) NOT NULL,
    device VARCHAR(255) NOT NULL,
    PRIMARY KEY(id),
    FOREIGN KEY(id_campo) REFERENCES campi (id),
    flag_eliminato int DEFAULT 0,
    data_eliminazione datetime DEFAULT NULL
);

CREATE TABLE processing_request(
    id INT(6) NOT NULL AUTO_INCREMENT,
    azione VARCHAR(30) NOT NULL,
    stato VARCHAR(30) NOT NULL,
    device VARCHAR(255) NOT NULL, 
    data_processing datetime DEFAULT NULL,
    data_inserimento datetime DEFAULT NULL,
    note VARCHAR(4000) NOT NULL,
    flag_eliminato int DEFAULT 0,
    data_eliminazione datetime DEFAULT NULL,
    PRIMARY KEY(id)
);

CREATE TABLE operation_temperatura(
    id INT(6) NOT NULL AUTO_INCREMENT,
    temperatura VARCHAR(30) NOT NULL,
    data_acquisizione datetime DEFAULT NULL,
    id_impianto INT(6) NOT NULL,
    flag_eliminato int DEFAULT 0,
    data_eliminazione datetime DEFAULT NULL,
    PRIMARY KEY(id),
    FOREIGN KEY(id_impianto) REFERENCES impianti (id)
);

CREATE TABLE operation_umidita(
    id INT(6) NOT NULL AUTO_INCREMENT,
    umidita VARCHAR(30) NOT NULL,
    data_acquisizione datetime DEFAULT NULL,
    id_impianto INT(6) NOT NULL,
    flag_eliminato int DEFAULT 0,
    data_eliminazione datetime DEFAULT NULL,
    PRIMARY KEY(id),
    FOREIGN KEY(id_impianto) REFERENCES impianti (id)
);


INSERT INTO `utenti` (`id`, `nome`, `cognome`, `password`, `email`, `telefono`, `amministratore`) VALUES ('1', 'admin', 'admin', 'password', 'admin@aatech.it', '33333333', 'si');

