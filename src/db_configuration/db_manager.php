<?php
include 'config.php';

class DbManager {
	
	private $mysqli = '';
	private $last_id = 0;
	private $conn_status = 0;
	private $conn_err = '';
	private $query_status = 0;
	private $query_err = '';

	public function __construct(){
		$this->connettiDb();
	}

	public function connettiDb(){
		$this->mysqli = new mysqli(DatiDb::db_server, DatiDb::db_username, DatiDb::db_password, DatiDb::db_name);
		if (!$this->mysqli->connect_errno){//connessione ok
			$this->conn_status = 1;
			$this->conn_err = '';
		}else{//problemi connessione
			$this->conn_status = 0;
			$this->conn_err = $this->mysqli->connect_error;
		}
	}

	public function eseguiQuery($query){
		if($this->conn_status == 0){return false;}
		$result = $this->mysqli->query($query);
		if(!$result){//problemi query
			$this->query_status = 0;
			$this->query_err = $this->mysqli->error;	
		}else{//query ok
			$this->query_status = 1;
			$this->query_err = '';	
			$this->last_id = $this->mysqli->insert_id;	
		}
		return $result;		
	}

	public function escapeString($str){ return $this->mysqli->real_escape_string($str);	}
    
	public function getConnStatus(){ return $this->conn_status; }

	public function getConnErr(){ return $this->conn_err;	}
	
	public function getQueryStatus(){ return $this->query_status;	}

	public function getQueryErr(){ return $this->query_err; }
	
	public function getLastId(){ return $this->last_id; }

    public function startTransaction(){ $this->mysqli->begin_transaction(); }

    public function commit(){ $this->mysqli->commit(); }

    public function rollback(){ $this->mysqli->rollback(); }
}
?>