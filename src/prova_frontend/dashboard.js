// PER IL GRAFICO temperatura
    google.charts.load('current', {packages: ['corechart', 'line']});
    google.charts.setOnLoadCallback(drawBasic);

    function drawBasic() {

        var data = new google.visualization.DataTable();
        data.addColumn('number', 'X');
        data.addColumn('number', 'Temperatura');

        data.addRows([
            [12, 25],   [14, 26],  [16, 23],  [18, 28],  [20, 25],  [22, 26],
        ]);

        var options = {
            hAxis: {
            title: 'Ore'
            },
            vAxis: {
            title: '°C'
            }
        };

        var chart = new google.visualization.LineChart(document.getElementById('chart_temp'));

        chart.draw(data, options);
        }




//-------- GRAFICO PH
google.charts.load('current', {'packages':['corechart']});
google.charts.setOnLoadCallback(drawChart);
function drawChart() {
    var data = google.visualization.arrayToDataTable([
      ['Ore', 'Ph'],
      ['12',  5],
      ['14',  6],
      ['16',  6],
      ['18',  6.5]
    ]);

    var options = {
      title: 'PH',
      hAxis: {title: 'Ore',  titleTextStyle: {color: '#333'}},
      vAxis: {minValue: 0}
    };

    var chart = new google.visualization.AreaChart(document.getElementById('chart_ph'));
    chart.draw(data, options);
  }


//   ----GRAFICO CONDUCIBILITÀ 
google.charts.load('current', {packages: ['corechart', 'line']});
google.charts.setOnLoadCallback(drawBasic2);

function drawBasic2() {

    var data = new google.visualization.DataTable();
    data.addColumn('number', 'X');
    data.addColumn('number', 'Temperatura');

    data.addRows([
        [12, 1000],   [14, 1100],  [16, 1198],  [18, 1200],  [20, 1890],  [22, 1890],
    ]);

    var options = {
        hAxis: {
        title: 'Ore'
        },
        vAxis: {
        title: 'uS'
        }
    };

    var chart = new google.visualization.LineChart(document.getElementById('chart_cond'));

    chart.draw(data, options);
    }


