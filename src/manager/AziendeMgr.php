    <?php

class AziendeMgr {

    private $db;

    //COSTRUTTORE
    public function __construct($dbMgr){
        $this->db=$dbMgr;
    }

    //Metodi generici
    private function isConnOk(){
        if($this->db->getConnStatus() == 0){return false;}
        return true;
    }

    private function isQueryOk(){
        if($this->db->getQueryStatus() == 0){return false;}
        return true;
    }
    public function escapeString($str){ 
        return $this->mysqli->real_escape_string($str);	
    }

// INSERT AZIENDA
public function insertAzienda($values)
{
    $ragione_sociale = $this->db->escapeString($values['ragione_sociale']);
    $indirizzo = $this->db->escapeString($values['indirizzo']);
    $telefono = $this->db->escapeString($values['telefono']);
    $codice_fiscale = $this->db->escapeString($values['codice_fiscale']);
    $partita_iva = $this->db->escapeString($values['partita_iva']);
    $id_utente = $this->db->escapeString($values['id_utente']);



    if(!$this->isConnOk()){
        throw new Exception("Errore di connessione al Database");
    }

    $sql = "INSERT INTO aziende(ragione_sociale, indirizzo, telefono, codice_fiscale, partita_iva, id_utente) VALUES ('$ragione_sociale','$indirizzo','$telefono','$codice_fiscale','$partita_iva','$id_utente')";

    $result = $this->db->eseguiQuery($sql);
    if(!$this->isQueryOk()){
        //var_dump($this->db->getQueryErr());
        throw new Exception($this->db->getQueryErr());
    }
    return $this->db->getLastId();
}

//DELETE AZIENDA
public function deleteAziendaById($id)
{
    $data=date('Y-m-d H:i:s');

		if(!$this->isConnOk()){ 
			return -1; 
		}

		$id = $this->db->escapeString($id);
		$result = $this->db->eseguiQuery("UPDATE aziende SET flag_eliminato=1, data_eliminazione='$data' WHERE id='$id'");

		if(!$this->isQueryOk()){ 
			return -1; 
		}	
		return 1;
}

//UPDATE AZIENDA
public function updateAziendaById($id, $values)
{
    $ragione_sociale = $this->db->escapeString($values['ragione_sociale']);
    $indirizzo = $this->db->escapeString($values['indirizzo']);
    $telefono = $this->db->escapeString($values['telefono']);
    $codice_fiscale = $this->db->escapeString($values['codice_fiscale']);
    $partita_iva = $this->db->escapeString($values['partita_iva']);
    $id_utente = $this->db->escapeString($values['id_utente']);



    if(!$this->isConnOk()){
        throw new Exception("Errore di connessione al Database");
    }

    $sql = "UPDATE aziende SET id_utente='$id_utente',ragione_sociale='$ragione_sociale', indirizzo='$indirizzo', telefono='$telefono', codice_fiscale='$codice_fiscale', partita_iva='$partita_iva' WHERE id=$id";
    $result = $this->db->eseguiQuery($sql);


    if(!$this->isQueryOk()){
        throw new Exception($this->db->getQueryErr());
    }
    return 1;
}


//Ricerca aziende per id
public function getAziendaById($id){

    if(!$this->isConnOk()){
        throw new Exception("Errore di connessione al Database");
    }

    $result = $this->db->eseguiQuery("SELECT * FROM aziende WHERE id='$id' AND flag_eliminato=0");

    if(!$this->isQueryOk()){
        throw new Exception($this->db->getQueryErr());
    }

    if(mysqli_num_rows($result) == 0){
        throw new Exception("Nessuna azienda trovata con quell'id");
    }

    while($row=mysqli_fetch_array($result,MYSQLI_ASSOC)){
        $dati[] = new Aziende($row);
    }
    


    return $dati;
}

    //Ricerca aziende per id utente
    public function getAziendaByIdUtente($id_utente){

        if(!$this->isConnOk()){
            throw new Exception("Errore di connessione al Database");
        }
        $result = $this->db->eseguiQuery("SELECT * FROM aziende WHERE id_utente = $id_utente AND flag_eliminato=0");

        if(!$this->isQueryOk()){
            throw new Exception($this->db->getQueryErr());
        }
        //var_dump($result);
        if(mysqli_num_rows($result) == 0){
            throw new Exception("Nessuna azienda trovata per id utente");
        }

        while($row=mysqli_fetch_array($result,MYSQLI_ASSOC)){
            $dati[] = new Aziende($row);
        }

        return $dati;
    }


//RICERCA DI TUTTE LE AZIENDE
public function getAllAziende(){
    $dati=[];
    if(!$this->isConnOk()){
        throw new Exception("Errore di connessione al Database");
    }

    $result = $this->db->eseguiQuery("SELECT * FROM aziende WHERE flag_eliminato=0");

    if(!$this->isQueryOk()){
        throw new Exception($this->db->getQueryErr());
    }

    while($row=mysqli_fetch_array($result,MYSQLI_ASSOC)){
        $dati[] = new Aziende($row);
    }
    return $dati;
}


}
?>