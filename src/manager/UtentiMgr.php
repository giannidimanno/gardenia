<?php

class UtentiMgr {

    private $db;

    //COSTRUTTORE
    public function __construct($dbMgr){
        $this->db=$dbMgr;
    }

    //Metodi generici
    private function isConnOk(){
        if($this->db->getConnStatus() == 0){return false;}
        return true;
    }

    private function isQueryOk(){
        if($this->db->getQueryStatus() == 0){return false;}
        return true;
    }


    //RICERCA DI TUTTI I utenti
    public function getAllUtenti(){

        if(!$this->isConnOk()){
            throw new Exception("Errore di connessione al Database");
        }

        $result = $this->db->eseguiQuery("SELECT * FROM utenti where flag_eliminato=0");

        if(!$this->isQueryOk()){
            throw new Exception($this->db->getQueryErr());
        }

        while($row=mysqli_fetch_array($result,MYSQLI_ASSOC)){
            $dati[] = new Utenti($row);
        }
        return $dati;
    }

    //RICERCA DI TUTTI I utenti
    public function getAllUtentiNotAdmin(){
        $dati=[];
        if(!$this->isConnOk()){
            throw new Exception("Errore di connessione al Database");
        }

        $result = $this->db->eseguiQuery("SELECT * FROM utenti where amministratore != 'si' AND flag_eliminato=0 ");

        if(!$this->isQueryOk()){
            throw new Exception($this->db->getQueryErr());
        }

        while($row=mysqli_fetch_array($result,MYSQLI_ASSOC)){
            $dati[] = new Utenti($row);
        }
        return $dati;
    }

    //RICERCA utente IN BASE AL COGNOME
    public function getUtentiByCognome($cognome){

        if(!$this->isConnOk()){
            throw new Exception("Errore di connessione al Database");
        }

        $result = $this->db->eseguiQuery("SELECT * FROM utenti WHERE cognome='$cognome' AND flag_eliminato=0");

        if(!$this->isQueryOk()){
            throw new Exception($this->db->getQueryErr());
        }

        if(mysqli_num_rows($result) == 0){
            throw new Exception("Non è stato trovato nessun utente con questo cognome");
        }

        while($row=mysqli_fetch_array($result,MYSQLI_ASSOC)){
            $dati[] = new Utenti($row);
        }

        return $dati;
    }


    //RICERCA utente IN BASE ALL'ID
    public function getUtentiById($id){

        if(!$this->isConnOk()){
            throw new Exception("Errore di connessione al Database");
        }

        $result = $this->db->eseguiQuery("SELECT * FROM utenti WHERE id='$id' AND flag_eliminato=0");

        if(!$this->isQueryOk()){
            throw new Exception($this->db->getQueryErr());
        }

        if(mysqli_num_rows($result) == 0){
            throw new Exception("Nessun utente trovato");
        }

        while($row=mysqli_fetch_array($result,MYSQLI_ASSOC)){
            $dati[] = new Utenti($row);
        }

        return $dati;
    }

    //CREA utente
    public function insertUtenti($values){

        $nome = $this->db->escapeString($values['nome']);
        $cognome = $this->db->escapeString($values['cognome']);
        $password = $this->db->escapeString($values['password']);
        $email = $this->db->escapeString($values['email']);
        $telefono = $this->db->escapeString($values['telefono']);
        $amministratore = $this->db->escapeString($values['amministratore']);


        if(!$this->isConnOk()){
            throw new Exception("Errore di connessione al Database");
        }


        $sql="INSERT INTO utenti(nome, cognome, password, email, telefono, amministratore) 
                values ('$nome','$cognome','$password','$email', '$telefono','$amministratore')";
        $result = $this->db->eseguiQuery($sql);

        if(!$this->isQueryOk()){
            throw new Exception($this->db->getQueryErr());
        }

        $id = ($this->db->getLastId());
        return $id;

    }


    //MODIFICA DATI utente
    public function updateUtenti($id, $values){

        $nome = $this->db->escapeString($values['nome']);
        $cognome = $this->db->escapeString($values['cognome']);
        $password = $this->db->escapeString($values['password']);
        $email = $this->db->escapeString($values['email']);
        $telefono = $this->db->escapeString($values['telefono']);
        $amministratore = $this->db->escapeString($values['amministratore']);


        if(!$this->isConnOk()){
            throw new Exception("Errore di connessione al Database");
        }
        $sql="UPDATE utenti 
                SET nome='$nome', cognome='$cognome', password='$password', email='$email', telefono='$telefono', amministratore='$amministratore' 
                WHERE id = $id";

        // var_dump($sql);die();    

        $result = $this->db->eseguiQuery($sql);

        if(!$this->isQueryOk()){
            throw new Exception($this->db->getQueryErr());
        }

        return 1;
    }


    //ELIMINA utente PER ID
    public function deleteUtenti($id){

        $data=date('Y-m-d H:i:s');

		if(!$this->isConnOk()){ 
			return -1; 
		}

		$id = $this->db->escapeString($id);
		$result = $this->db->eseguiQuery("UPDATE utenti SET flag_eliminato=1, data_eliminazione='$data' WHERE id='$id'");

		if(!$this->isQueryOk()){ 
			return -1; 
		}	
		return 1;
	}

    // LOGIN
    public function getLogin($email,$password){

        if(!$this->isConnOk()){
            throw new Exception("Errore di connessione al Database");
        }

        $email = $this->db->escapeString($email);
        $password = $this->db->escapeString($password);

        $result = $this->db->eseguiQuery("SELECT * FROM utenti WHERE email='$email' and password='$password' AND flag_eliminato=0 ");

        if(!$this->isQueryOk()){
            throw new Exception($this->db->getQueryErr());
        }

        if(mysqli_num_rows($result) == 0){
            throw new Exception("Nessun utente trovato");
        }

        while($row=mysqli_fetch_array($result,MYSQLI_ASSOC)){
            $dati[] = new Utenti($row);
        }

        return $dati;
    }

    public function checkEmailExist($email){

        if(!$this->isConnOk()){
            throw new Exception("Errore di connessione al Database");
        }

        $email = $this->db->escapeString($email);
        $result = $this->db->eseguiQuery("SELECT email FROM utenti WHERE email='$email' AND flag_eliminato=0");

        if(!$this->isQueryOk()){
            throw new Exception($this->db->getQueryErr());
        }

        if(mysqli_num_rows($result) == 0){
            return false;
        }else{
            return true;
        }

    }



}

?>