<?php

class CampiMgr {

    private $db;

    //COSTRUTTORE
    public function __construct($dbMgr){
        $this->db=$dbMgr;
    }

    //Metodi generici
    private function isConnOk(){
        if($this->db->getConnStatus() == 0){return false;}
        return true;
    }

    private function isQueryOk(){
        if($this->db->getQueryStatus() == 0){return false;}
        return true;
    }


    public function insertCampo($values){

        $luogo = $this->db->escapeString($values['luogo']);
        $dimensione = $this->db->escapeString($values['dimensione']);
        $latitudine = $values ['latitudine'];
        $longitudine = $values ['longitudine'];
        $id_azienda = $values['id_azienda'];

        if(!$this->isConnOk()){
            throw new Exception("Errore di connessione al Database");
        }

        $sql="INSERT INTO campi(luogo,dimensione, latitudine, longitudine, id_azienda) values ('$luogo', '$dimensione', $latitudine, $longitudine,$id_azienda)";
        $result = $this->db->eseguiQuery($sql);
        if(!$this->isQueryOk()){
            throw new Exception($this->db->getQueryErr());
        }

        $id = ($this->db->getLastid());
        return $id;

    }



    //Modifica dei settaggi
    public function updateCampoById($id, $values){

        $luogo = $this->db->escapeString($values['luogo']);
        $dimensione = $this->db->escapeString($values['dimensione']);
        $id_azienda = $values['id_azienda'];

        $sql="UPDATE campi set id_azienda='$id_azienda',luogo='$luogo',dimensione='$dimensione' where id=$id";
        if(!$this->isConnOk()){
            throw new Exception("Errore di connessione al Database");
        }

        $result = $this->db->eseguiQuery($sql);

        if(!$this->isQueryOk()){
            throw new Exception($this->db->getQueryErr());
        }

        return 1;
    }



    //Ricerca settaggi per id
    public function getCampoById($id){

        if(!$this->isConnOk()){
            throw new Exception("Errore di connessione al Database");
        }
        $result = $this->db->eseguiQuery("SELECT * FROM campi WHERE id=$id AND flag_eliminato=0");

        if(!$this->isQueryOk()){
            throw new Exception($this->db->getQueryErr());
        }

        if(mysqli_num_rows($result) == 0){
            throw new Exception("Nessuna preferenza settata per id campo");
        }

        while($row=mysqli_fetch_array($result,MYSQLI_ASSOC)){
            $dati[] = new Campi($row);
        }

        return $dati;
    }

    //Ricerca settaggi per id
    public function getCampiByIdUtente($id_utente){

        if(!$this->isConnOk()){
            throw new Exception("Errore di connessione al Database");
        }
        $result = $this->db->eseguiQuery("SELECT campi.* FROM campi,aziende 
                                            where campi.id_azienda=aziende.id
                                            and aziende.id_utente=$id_utente
                                            AND campi.flag_eliminato=0");

        if(!$this->isQueryOk()){
            throw new Exception($this->db->getQueryErr());
        }

        if(mysqli_num_rows($result) == 0){
            throw new Exception("Nessun campo trovato per id utente");
        }

        while($row=mysqli_fetch_array($result,MYSQLI_ASSOC)){
            $dati[] = new Campi($row);
        }

        return $dati;
    }


    //Ricerca settaggi per id_azienda
    public function getCampiByIdAzienda($id_azienda){

        if(!$this->isConnOk()){
            throw new Exception("Errore di connessione al Database");
        }
        $result = $this->db->eseguiQuery("SELECT campi.* FROM campi,aziende 
                                            where campi.id_azienda=$id_azienda
                                            AND campi.flag_eliminato=0");

        if(!$this->isQueryOk()){
            throw new Exception($this->db->getQueryErr());
        }

        if(mysqli_num_rows($result) == 0){
            throw new Exception("Nessun campo trovato per id azienda");
        }

        while($row=mysqli_fetch_array($result,MYSQLI_ASSOC)){
            $dati[] = new Campi($row);
        }

        return $dati;
    }




    //Elimina professione per id
    public function deleteById($id){

        $data=date('Y-m-d H:i:s');

		if(!$this->isConnOk()){ 
			return -1; 
		}

		$id = $this->db->escapeString($id);
		$result = $this->db->eseguiQuery("UPDATE campi SET flag_eliminato=1, data_eliminazione='$data' WHERE id='$id'");

		if(!$this->isQueryOk()){ 
			return -1; 
		}	
		return 1;
    }



        //RICERCA DI TUTTI I CAMPI
        public function getAllCampi(){
            $dati=[];
            if(!$this->isConnOk()){
                throw new Exception("Errore di connessione al Database");
            }
    
            $result = $this->db->eseguiQuery("SELECT * FROM campi WHERE flag_eliminato=0");
    
            if(!$this->isQueryOk()){
                throw new Exception($this->db->getQueryErr());
            }
    
            while($row=mysqli_fetch_array($result,MYSQLI_ASSOC)){
                $dati[] = new Campi($row);
            }
            return $dati;
        }
    

}


?>
