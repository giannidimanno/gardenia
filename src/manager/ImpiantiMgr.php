<?php

class ImpiantiMgr {

    private $db;

    //COSTRUTTORE
    public function __construct($dbMgr){
        $this->db=$dbMgr;
    }

    //Metodi generici
    private function isConnOk(){
        if($this->db->getConnStatus() == 0){return false;}
        return true;
    }

    private function isQueryOk(){
        if($this->db->getQueryStatus() == 0){return false;}
        return true;
    }
    public function escapeString($str){ 
        return $this->mysqli->real_escape_string($str);	
    }

    

    public function insertImpianto($values)
    {
        $nome = $this->db->escapeString($values['nome']);
        $dimensioni = $this->db->escapeString($values['dimensioni']);
        $stato = $values['stato'];
        $id_campo=$values['id_campo'];
        $device=$values['device'];
        

        if(!$this->isConnOk()){
            throw new Exception("Errore di connessione al Database");
        }

        $sql = "INSERT INTO impianti (nome, dimensioni, stato, id_campo, device) VALUES ('$nome', '$dimensioni', $stato, $id_campo, '$device')";
        $result = $this->db->eseguiQuery($sql);

        if(!$this->isQueryOk()){
            throw new Exception($this->db->getQueryErr());
        }
        return $this->db->getLastId();
    }


    public function deleteImpiantiById($id)
    {
        $data=date('Y-m-d H:i:s');

		if(!$this->isConnOk()){ 
			return -1; 
		}

		$id = $this->db->escapeString($id);
		$result = $this->db->eseguiQuery("UPDATE impianti SET flag_eliminato=1, data_eliminazione='$data' WHERE id='$id'");

		if(!$this->isQueryOk()){ 
			return -1; 
		}	
		return 1;
    }


//Update impianti

    public function updateImpiantoById($values, $id)
    {
        $nome = $this->db->escapeString($values['nome']);
        $dimensioni = $this->db->escapeString($values['dimensioni']);
        $stato = $values['stato'];
        $id_campo=$values['id_campo'];
        $device=$values['device'];
        if(!$this->isConnOk()){
            throw new Exception("Errore di connessione al Database");
        }

        $sql = "UPDATE impianti SET nome='$nome', dimensioni='$dimensioni', stato='$stato', id_campo=$id_campo, device='$device' WHERE id=$id";
        $result = $this->db->eseguiQuery($sql);


        if(!$this->isQueryOk()){
            throw new Exception($this->db->getQueryErr());
        }
        return 1;
    }


    public function getAllImpianti(){
        $dati=[];
        if(!$this->isConnOk()){
            throw new Exception("Errore di connessione al Database");
        }

        $result = $this->db->eseguiQuery("SELECT * FROM impianti WHERE flag_eliminato=0");

        if(!$this->isQueryOk()){
            throw new Exception($this->db->getQueryErr());
        }

        while($row=mysqli_fetch_array($result,MYSQLI_ASSOC)){
            $dati[] = new impianti($row);
        }

        return $dati;
    }

    public function getImpiantiById($id){

        if(!$this->isConnOk()){
            throw new Exception("Errore di connessione al Database");
        }

        $result = $this->db->eseguiQuery("SELECT * FROM impianti WHERE id='$id' AND flag_eliminato=0");

        if(!$this->isQueryOk()){
            throw new Exception($this->db->getQueryErr());
        }

        if(mysqli_num_rows($result) == 0){
            throw new Exception("Nessuna impianti
             trovata con quell'id");
        }

        while($row=mysqli_fetch_array($result,MYSQLI_ASSOC)){
            $dati[] = new impianti($row);
        }
        


        return $dati;
    }

    //Ricerca impianti per id utente
    public function getImpiantiByIdUtente($id_utente){
        $dati=[];
        if(!$this->isConnOk()){
            throw new Exception("Errore di connessione al Database");
        }

        $sql="SELECT impianti.* FROM impianti,campi,aziende 
            where impianti.id_campo=campi.id
            and campi.id_azienda=aziende.id
            and aziende.id_utente=$id_utente
            AND impianti.flag_eliminato=0";

        $result = $this->db->eseguiQuery($sql);

        if(!$this->isQueryOk()){
            throw new Exception($this->db->getQueryErr());
        }

        if(mysqli_num_rows($result) == 0){
            throw new Exception("Nessuna impianti
             trovata per id utente");
        }

        while($row=mysqli_fetch_array($result,MYSQLI_ASSOC)){
            $dati[] = new Impianti($row);
        }

        return $dati;
    }

    //Ricerca impianti per id utente
    public function getImpiantiByIdCampo($id_campo){
        $dati=[];
        if(!$this->isConnOk()){
            throw new Exception("Errore di connessione al Database");
        }

        $sql="SELECT impianti.* FROM impianti,campi 
            where impianti.id_campo=$id_campo
            AND impianti.flag_eliminato=0";

        $result = $this->db->eseguiQuery($sql);

        if(!$this->isQueryOk()){
            throw new Exception($this->db->getQueryErr());
        }

        if(mysqli_num_rows($result) == 0){
            throw new Exception("Nessun impianto
             trovato per id campo");
        }

        while($row=mysqli_fetch_array($result,MYSQLI_ASSOC)){
            $dati[] = new Impianti($row);
        }

        return $dati;
    }


    //Ricerca impianti per id utente
    public function getImpiantiByIdAzienda($id_azienda){
        $dati=[];
        if(!$this->isConnOk()){
            throw new Exception("Errore di connessione al Database");
        }

        $sql="SELECT impianti.* FROM impianti,campi,aziende 
            where impianti.id_campo=campi.id
            and campi.id_azienda=aziende.id
            and aziende.id=$id_azienda
            AND impianti.flag_eliminato=0";

        $result = $this->db->eseguiQuery($sql);

        if(!$this->isQueryOk()){
            throw new Exception($this->db->getQueryErr());
        }

        if(mysqli_num_rows($result) == 0){
            throw new Exception("Nessun impianto
             trovato per id campo");
        }

        while($row=mysqli_fetch_array($result,MYSQLI_ASSOC)){
            $dati[] = new Impianti($row);
        }

        return $dati;
    }

    public function getImpiantiByIdDevice($device){
        $dati=[];
        if(!$this->isConnOk()){
            throw new Exception("Errore di connessione al Database");
        }

        $sql="SELECT * FROM impianti
            where device='$device'
            AND flag_eliminato=0";
        $result = $this->db->eseguiQuery($sql);

        if(!$this->isQueryOk()){
            throw new Exception($this->db->getQueryErr());
        }

        if(mysqli_num_rows($result) == 0){
            throw new Exception("Nessun impianto
             trovato per questo device");
        }

        while($row=mysqli_fetch_array($result,MYSQLI_ASSOC)){
            $dati[] = new Impianti($row);
        }

        return $dati;
    }


}

?>