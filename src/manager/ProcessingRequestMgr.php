<?php

    class ProcessingRequestMgr{

        private $db;

    //COSTRUTTORE
    public function __construct($dbMgr){
        $this->db=$dbMgr;
    }

    //Metodi generici
    private function isConnOk(){
        if($this->db->getConnStatus() == 0){return false;}
        return true;
    }

    private function isQueryOk(){
        if($this->db->getQueryStatus() == 0){return false;}
        return true;
    }
    public function escapeString($str){ 
        return $this->mysqli->real_escape_string($str);	
    }

//INSERT 

    public function insertProcessingRequest($values)
    {
        $data=date('Y-m-d H:i:s');

        $azione = $this->db->escapeString($values['azione']);
        $stato = $this->db->escapeString($values['stato']);
        $device = $this->db->escapeString($values['device']);
        $data_processing = $values['data_processing'];
        $data_inserimento = $data;
        $note = $values['note'];

        
        

        if(!$this->isConnOk()){
            throw new Exception("Errore di connessione al Database");
        }

        $sql = "INSERT INTO processing_request (azione, stato, device, data_inserimento, note) 
                VALUES ('$azione', '$stato', '$device', '$data_inserimento', '$note')";
    echo $sql;
        $result = $this->db->eseguiQuery($sql);

        if(!$this->isQueryOk()){
            throw new Exception($this->db->getQueryErr());
        }
        return $this->db->getLastId();
    }
    
//UPDATE BY ID
    public function updateProcessingRequestById($values, $id)
    {
        $azione = $this->db->escapeString($values['azione']);
        $stato = $this->db->escapeString($values['stato']);
        $device = $values['device'];
        $data_processing=$values['data_processing'];
        $data_inserimento=$values['data_inserimento'];
        $note=$values['note'];
        if(!$this->isConnOk()){
            throw new Exception("Errore di connessione al Database");
        }

        $sql = "UPDATE processing_request SET azione='$azione', stato='$stato', device=$device, data_processing='$data_processing', data_inserimento='$data_inserimento', note = '$note' WHERE id=$id";
        $result = $this->db->eseguiQuery($sql);


        if(!$this->isQueryOk()){
            throw new Exception($this->db->getQueryErr());
        }
        return 1;
    }

//DELETE (da fare ancora i modals)

    public function deleteProcessingRequestById($id)
    {
        $data=date('Y-m-d H:i:s');

        if(!$this->isConnOk()){ 
            return -1; 
        }

        $id = $this->db->escapeString($id);
        $result = $this->db->eseguiQuery("UPDATE processing_request SET flag_eliminato=1, data_eliminazione='$data' WHERE id='$id'");

        if(!$this->isQueryOk()){ 
            return -1; 
        }	
        return 1;
    }


// GET BY DEVICE

    public function getProcessingRequestByDevice($device){

        if(!$this->isConnOk()){
            throw new Exception("Errore di connessione al Database");
        }

        $result = $this->db->eseguiQuery("SELECT * FROM processing_request WHERE device='$device' AND flag_eliminato=0 AND stato = 'TO_DO'");
        if(!$this->isQueryOk()){
            throw new Exception($this->db->getQueryErr());
        }

        if(mysqli_num_rows($result) == 0){
            throw new Exception("Nessuna processing_request
            trovata con quell'device");
        }

        while($row=mysqli_fetch_array($result,MYSQLI_ASSOC)){
            $dati[] = $row;
        }

        return $dati;
    }

//UPDATE STATUS 
    public function updateProcessingRequestStatoById($stato, $note, $id)
    {

        $data=date('Y-m-d H:i:s');

        if(!$this->isConnOk()){
            throw new Exception("Errore di connessione al Database");
        }

        $sql = "UPDATE processing_request SET stato='$stato', data_processing='$data', note='$note'  WHERE id=$id";
        $result = $this->db->eseguiQuery($sql);


        if(!$this->isQueryOk()){
            throw new Exception($this->db->getQueryErr());
        }
        return 1;
    }

    }
?>