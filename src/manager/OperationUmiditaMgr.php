<?php

    class OperationUmiditaMgr{

        private $db;

    //COSTRUTTORE
    public function __construct($dbMgr){
        $this->db=$dbMgr;
    }

    //Metodi generici
    private function isConnOk(){
        if($this->db->getConnStatus() == 0){return false;}
        return true;
    }

    private function isQueryOk(){
        if($this->db->getQueryStatus() == 0){return false;}
        return true;
    }
    public function escapeString($str){ 
        return $this->mysqli->real_escape_string($str);	
    }

//INSERT    

    public function insertOperationUmidita($values)
    {
        $data=date('Y-m-d H:i:s');
        $umidita = $this->db->escapeString($values['umidita']);
        $id_impianto = $this->db->escapeString($values['id_impianto']);


        if(!$this->isConnOk()){
            throw new Exception("Errore di connessione al Database");
        }

        $sql = "INSERT INTO operation_umidita (umidita, data_acquisizione, id_impianto) 
                VALUES ('$umidita', '$data', '$id_impianto')";

        $result = $this->db->eseguiQuery($sql);

        if(!$this->isQueryOk()){
            throw new Exception($this->db->getQueryErr());
        }
        return $this->db->getLastId();
    }


    
    public function getAllOperationUmiditaIn30Days($id_impianto){
        $dati=[];
        if(!$this->isConnOk()){
            throw new Exception("Errore di connessione al Database");
        }

        $result = $this->db->eseguiQuery("SELECT * FROM  operation_umidita WHERE id_impianto=$id_impianto and  flag_eliminato=0 order by 1 desc LIMIT 30");

        if(!$this->isQueryOk()){
            throw new Exception($this->db->getQueryErr());
        }

        while($row=mysqli_fetch_array($result,MYSQLI_ASSOC)){
            $dati[] = new impianti($row);
        }

        return $dati;
    }

//DELETE (da fare ancora i modals)

    public function deleteOperationUmiditaById($id)
    {
        $data=date('Y-m-d H:i:s');

        if(!$this->isConnOk()){ 
            return -1; 
        }

        $id = $this->db->escapeString($id);
        $result = $this->db->eseguiQuery("UPDATE operation_umidita SET flag_eliminato=1, data_eliminazione='$data' WHERE id='$id'");

        if(!$this->isQueryOk()){ 
            return -1; 
        }	
        return 1;
    }


    }
?>