// function initMap() {

//   const map = new google.maps.Map(document.getElementById("map"), {
//     center: { lat: 42.70875990841038, lng: 12.47928491439982 },
//     zoom: 6,
//   });

//   const drawingManager = new google.maps.drawing.DrawingManager({
//     drawingMode: google.maps.drawing.OverlayType.POLYGON,
//     drawingControl: true,
//     drawingControlOptions: {
//       position: google.maps.ControlPosition. LEFT_CENTER,
//       drawingModes: [
//         google.maps.drawing.OverlayType.MARKER,
//         // google.maps.drawing.OverlayType.CIRCLE,
//         google.maps.drawing.OverlayType.POLYGON,
//         google.maps.drawing.OverlayType.POLYLINE,
//         google.maps.drawing.OverlayType.RECTANGLE,
//       ],
//     },
//     polygonOptions: {
//       fillColor: "#6dec30",
//       fillOpacity: 0.3,
//       strokeWeight: 1,
//       clickable: true,
//       editable: true,
//       zIndex: 1,
//     }
//   });
//   drawingManager.setMap(map);
// }




// ----------------------------------PROVa1-------------------------------------------------

// PER LEGGERE UN FILE JSON CON LE CORDINATE
// let map;

// function initMap() {
//   map = new google.maps.Map(document.getElementById("map"), {
//     zoom: 4,
//     center: { lat: -28, lng: 137 },
//   });
//   // NOTE: This uses cross-domain XHR, and may not work on older browsers.
//   map.data.loadGeoJson(
//     "json_prova.json"
//   );

//   console.log(map.data.loadGeoJson);
// }



// ------------------PROVA2 OTTENERE COORDINATE VERTICI DEI POLIGONI------------------------------------------------

var polygonArray =[];/*  array vuoto che conterrà le coordinate del poligono */
function initMap() {

  const map = new google.maps.Map(document.getElementById("map"), {
    center: { lat: 42.70875990841038, lng: 12.47928491439982 },
    zoom: 12,
  });

  const drawingManager = new google.maps.drawing.DrawingManager({
    drawingMode: google.maps.drawing.OverlayType.POLYGON,
    markerOptions: {
      draggable: true
    },
    polygonOptions: {
      draggable: true,
      clickable: true,
      editable: true,
      paths: true,

    },
    map: map,

    
  });  
  
  drawingManager.setMap(map);
  
 
  google.maps.event.addListener(drawingManager, 'polygoncomplete', function(polygon) {

    // assuming you want the points in a div with id="info"
    // document.getElementById('info').innerHTML += "polygon points:" + "<br>";
    for (var i = 0; i < polygon.getPath().getLength(); i++) {
        // document.getElementById('info').innerHTML += polygon.getPath().getAt(i).toJSON() + "<br>";
        var poly = polygon.getPath().getAt(i).toJSON();
        polygonArray.push(poly);
    }

    var jsonPoly = JSON.stringify(polygonArray);


    console.log(jsonPoly);

  });

  
}




